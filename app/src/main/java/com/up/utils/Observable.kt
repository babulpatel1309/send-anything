package com.up.utils

interface Observable<T>{
    fun onValueChange(obj: T)
}