package com.up.utils;

import android.os.AsyncTask;

import com.up.constants.AppheartKt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class Downloader
{
    DownloadUpdates mDownloadUpdates;
    String downloadUrl = "";
    String fileName = "";

    public Downloader(DownloadUpdates downloadUpdates, String downloadUrl, String fileName)
    {
        mDownloadUpdates = downloadUpdates;
        this.downloadUrl = downloadUrl;
        this.fileName = fileName;

        new DownloadFileAsync().execute();
    }

    //this is our download file asynctask
    public class DownloadFileAsync extends AsyncTask<String, String, String>
    {

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... aurl)
        {

            try
            {
                //connecting to url
                URL u = new URL(downloadUrl);
                HttpURLConnection c = (HttpURLConnection) u.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.setDoOutput(true);
                c.setRequestProperty("User-Agent", "Mozilla/5.0 ( compatible ) ");
                c.setRequestProperty("Accept", "*/*");
                c.connect();

                //lenghtOfFile is used for calculating download progress
                int lenghtOfFile = c.getContentLength();

                //this is where the file will be seen after the download
                FileOutputStream f = new FileOutputStream(new File(AppheartKt.docPath, fileName));

                int code = c.getResponseCode();
                //file input is from the url
                InputStream in;
                if (code == 200 || code == 201)
                {
                    in = c.getInputStream();
                }
                else
                {
                    in = c.getErrorStream();
                }

                //here’s the download code
                byte[] buffer = new byte[1024];
                int len1 = 0;
                long total = 0;

                BufferedReader theReader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
                StringBuilder response = new StringBuilder();
                String reply;
                while ((reply = theReader.readLine()) != null) {
                    response.append(reply);
                }

                String finalUrl = response.toString();

                while ((len1 = in.read(buffer)) > 0)
                {
                    total += len1; //total = total + len1
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    f.write(buffer, 0, len1);
                }
                f.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            return null;
        }

        protected void onProgressUpdate(String... progress)
        {
            mDownloadUpdates.downloadProgress(Integer.parseInt(progress[0]));
        }

        @Override
        protected void onPostExecute(String unused)
        {
        }
    }

    public interface DownloadUpdates
    {
        void downloadProgress(int progress);
    }
}
