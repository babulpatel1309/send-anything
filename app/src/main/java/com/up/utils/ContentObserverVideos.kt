package com.up.utils

import android.content.Context
import android.database.ContentObserver
import android.net.Uri

class ContentObserverVideos(var context: Context, var operationResponder: OperationResponder) : ContentObserver(null) {

    override fun onChange(selfChange: Boolean) {
        super.onChange(selfChange)
    }

    override fun onChange(selfChange: Boolean, uri: Uri?) {
        if (!selfChange && uri?.path != null && uri.path?.trim()?.length!! > 0) operationResponder.OnComplete()
    }

}