package com.up.main

import android.app.Activity
import android.util.Log
import com.bumptech.glide.Glide
import com.up.ApplicationClass
import com.up.R
import com.up.constants.*
import kotlinx.android.synthetic.main.fragment_code.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CodeActivity : BaseActivity() {

    var code: String? = ""

    override fun setContentView(): Int {
        return R.layout.fragment_code
    }

    override fun init() {
        code = ApplicationClass.app.getPrefs()?.getStringDetail(PARAM_CODE)
        fetchQR()
        makeTransferReady()
    }

    override fun buttonClicks() {
        btnClose.setOnClickListener {
            finish()
        }
    }

    fun fetchQR() {

        txtCode.text = code

        Glide.with(this)
                .load(ApiUrl.getInstance().getAPI(LOAD_QR_CODE))
                .into(imgQR)
    }

    private fun makeTransferReady() {

        Log.e("CODE", getDynamicCode())
        callWS()?.makeTransfer(ApiUrl.getInstance().getAPI(MAKE_TRANSFER_READY))?.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                startUploading(Activity.RESULT_CANCELED)
                toast("Request Timeout")
                Log.e("Error", "TimeOut")
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                Log.e("Verbose", "File transfer created with code- ${response.code()}")
                if (response.isSuccessful && isSuccess(response.code())) {
                    startUploading(Activity.RESULT_OK)
                }
            }
        })
    }

    fun startUploading(result: Int) {
        setResult(result)
        finish()
    }
}