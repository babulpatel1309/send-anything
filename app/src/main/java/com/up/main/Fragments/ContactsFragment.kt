package com.up.main.Fragments

import android.view.View
import com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager
import com.up.R
import com.up.adapter.StickyHeaderAdapterContacts
import com.up.constants.CONTACTS
import com.up.main.BaseActivity
import com.up.main.DashboardActivity
import com.up.utils.OperationResponder
import com.up.utils.SpacesItemDecoration
import kotlinx.android.synthetic.main.fragment_photos.view.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.dimen
import kotlin.collections.ArrayList

class ContactsFragment : BaseFragment() {

    var stickyHeaderAdapter: StickyHeaderAdapterContacts? = null

    lateinit var gridLayoutManager: StickyHeaderGridLayoutManager
    lateinit var parentActivity: DashboardActivity


    override fun setContentView(): Int {
        return R.layout.fragment_photos
    }

    override fun init() {
        parentActivity = mContext as DashboardActivity

        stickyHeaderAdapter = StickyHeaderAdapterContacts(mContext, parentActivity.adapterItemSelectionListener)

        gridLayoutManager = StickyHeaderGridLayoutManager(1)
        gridLayoutManager.setHeaderBottomOverlapMargin(resources.getDimension(R.dimen._5sdp).toInt())
        currentView.recyclerMain.layoutManager = gridLayoutManager
        currentView.recyclerMain.adapter = stickyHeaderAdapter
        currentView.recyclerMain.addItemDecoration(SpacesItemDecoration(mContext.dimen(R.dimen._100sdp)))

    }

    override fun buttonClicks() {
    }

    fun retrievePermission() {

        askPermission(mContext, object : BaseActivity.PermissionListener {
            override fun onGranted() {
                parentActivity.fetchAllContacts(object : OperationResponder {
                    override fun OnComplete(returnValue: Any?) {

                        if (returnValue != null) {
                            val allContacts = returnValue as ArrayList<DashboardActivity.MainDataList>
                            stickyHeaderAdapter?.refreshData(allContacts)
                        }
                        toggleViews()
                    }
                })
            }

            override fun onDenied() {
            }

        }, CONTACTS)

    }

    var fragmentVisible = false

    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)

        fragmentVisible = menuVisible
        if (menuVisible) {
            retrievePermission()
        }
    }

    fun toggleViews() {
        async(UI) {
            if (fragmentVisible) {
                currentView.progressLoad.visibility = View.GONE
                currentView.txtProgress.visibility = View.GONE
                currentView.progressResources.visibility = View.GONE

                if (parentActivity.dataModelContacts.isEmpty()) currentView.txtNoRecord.visibility = View.VISIBLE else currentView.txtNoRecord.visibility = View.GONE
            }
        }
    }

}