package com.up.main.Fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.up.R
import com.up.main.TestActivity
import kotlinx.android.synthetic.main.test_fragment_1.view.*

class TestFragment2 : Fragment() {

    lateinit var rootView: View
    lateinit var mContext: Context
    lateinit var testActivity: TestActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        highLight("Create")
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = context!!
        highLight("Attach")
    }

    override fun onStart() {
        super.onStart()
        highLight("Start")
    }

    override fun onAttachFragment(childFragment: Fragment?) {
        super.onAttachFragment(childFragment)
        highLight("AttachFragment")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        highLight("CreateView")
        rootView = inflater.inflate(R.layout.test_fragment_1, container, false)
        rootView.button.text = "Fragment 2"

        testActivity = activity as TestActivity

        rootView.button.setOnClickListener {
            testActivity.addFragment(TestFragment3())
        }

        return rootView
    }

    override fun onResume() {
        super.onResume()

        highLight("Resume")
    }

    override fun onPause() {
        super.onPause()
        highLight("Pause")
    }

    override fun onStop() {
        super.onStop()
        highLight("Stop")
    }

    override fun onDestroy() {
        super.onDestroy()
        highLight("Destroy")
    }

    override fun onDetach() {
        super.onDetach()
        highLight("Detach")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        highLight("DestroyView")
    }

    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)

        highLight("$menuVisible Visible")
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)

        highLight("$isVisibleToUser Hint")
    }

    fun highLight(msg: String) {
        Log.e("Fragment2 : ", msg)
    }
}