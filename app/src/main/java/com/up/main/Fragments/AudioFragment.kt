package com.up.main.Fragments

import android.support.v7.widget.RecyclerView
import android.view.View
import com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager
import com.up.R
import com.up.adapter.StickyHeaderAdapterAudio
import com.up.main.DashboardActivity
import com.up.utils.SpacesItemDecoration
import kotlinx.android.synthetic.main.fragment_photos.view.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.dimen

class AudioFragment : BaseFragment() {

    lateinit var gridLayoutManager: StickyHeaderGridLayoutManager
    lateinit var parentActivity: DashboardActivity

    var stickyHeaderAdapter: StickyHeaderAdapterAudio? = null
    var scrollIdle = true

    override fun setContentView(): Int {
        return R.layout.fragment_photos
    }

    override fun init() {

        parentActivity = mContext as DashboardActivity

        stickyHeaderAdapter = StickyHeaderAdapterAudio(mContext, parentActivity.adapterItemSelectionListener, this)

        gridLayoutManager = StickyHeaderGridLayoutManager(1)
        gridLayoutManager.setHeaderBottomOverlapMargin(resources.getDimension(R.dimen._2sdp).toInt())
        currentView.recyclerMain.layoutManager = gridLayoutManager
        currentView.recyclerMain.adapter = stickyHeaderAdapter
        currentView.recyclerMain.addItemDecoration(SpacesItemDecoration(mContext.dimen(R.dimen._100sdp)))

        val scrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {

                when (newState) {
                    RecyclerView.SCROLL_STATE_IDLE -> {
                        scrollIdle = true
                        stickyHeaderAdapter?.notifyAllSectionsDataSetChanged()
                    }
                    else -> {
                        scrollIdle = false
                    }

                }
            }

        }

        currentView.recyclerMain.addOnScrollListener(scrollListener)
    }

    override fun buttonClicks() {
    }

    fun onMimeTypeChange() {
        if (fragmentVisible) {
            stickyHeaderAdapter?.refreshData(parentActivity.dataModelAudio)
            toggleViews()
        }
    }

    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)
        fragmentVisible = menuVisible
        if (fragmentVisible) {
            onMimeTypeChange()
        }
    }

    var fragmentVisible = false

    fun toggleViews() {
        async(UI) {
            currentView.progressLoad.visibility = View.GONE
            currentView.txtProgress.visibility = View.GONE
            currentView.progressResources.visibility = View.GONE
            if (parentActivity.dataModelAudio.isEmpty()) currentView.txtNoRecord.visibility = View.VISIBLE else currentView.txtNoRecord.visibility = View.GONE
        }
    }
}