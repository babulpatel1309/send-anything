package com.up.main.Fragments

import android.support.v7.widget.RecyclerView
import android.view.View
import com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager
import com.up.R
import com.up.adapter.StickyHeaderAdapter
import com.up.main.DashboardActivity
import com.up.utils.SpacesItemDecoration
import kotlinx.android.synthetic.main.fragment_photos.view.*
import org.jetbrains.anko.dimen

class PhotosFragment : BaseFragment() {

    var stickyHeaderAdapter: StickyHeaderAdapter? = null

    lateinit var gridLayoutManager: StickyHeaderGridLayoutManager
    lateinit var parentActivity: DashboardActivity

    var isRefreshing = false

    override fun setContentView(): Int {
        return R.layout.fragment_photos
    }

    override fun init() {

        parentActivity = mContext as DashboardActivity
        stickyHeaderAdapter = StickyHeaderAdapter(mContext, parentActivity.adapterItemSelectionListener)

        gridLayoutManager = StickyHeaderGridLayoutManager(4)
        gridLayoutManager.setHeaderBottomOverlapMargin(resources.getDimension(R.dimen._2sdp).toInt())
        currentView.recyclerMain.layoutManager = gridLayoutManager
        currentView.recyclerMain.adapter = stickyHeaderAdapter
        currentView.recyclerMain.addItemDecoration(SpacesItemDecoration(mContext.dimen(R.dimen._100sdp)))


        val scrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {

                when (newState) {
                    RecyclerView.SCROLL_STATE_IDLE -> {
                        parentActivity.scrollIdle = true
                        stickyHeaderAdapter?.notifyAllSectionsDataSetChanged()
                    }
                    else -> {
                        parentActivity.scrollIdle = false
                    }

                }
            }

            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
            }
        }
        currentView.recyclerMain.addOnScrollListener(scrollListener)

    }

    override fun buttonClicks() {
    }

    var isRefreshingData = false
    fun onMimeTypeChange() {
        if (fragmentVisible && !isRefreshingData) {
            isRefreshingData = true
            stickyHeaderAdapter?.refreshData(parentActivity.dataModelImages)
            toggleViews()
            baseActivity.highLight("Refreshed ${parentActivity.dataModelImages}")
            isRefreshingData = false
        }
    }

    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)
        fragmentVisible = menuVisible

        if (fragmentVisible && isFragmentInitialized() && !isRefreshingData) onMimeTypeChange()
    }

    var fragmentVisible = false

    fun toggleViews(refreshOnly: Boolean = false) {
        currentView.progressLoad.visibility = View.GONE
        currentView.progressResources.visibility = View.GONE
        currentView.txtProgress.visibility = View.GONE
        if (parentActivity.dataModelImages.isEmpty()) currentView.txtNoRecord.visibility = View.VISIBLE else currentView.txtNoRecord.visibility = View.GONE

        if (refreshOnly || isRefreshing) {
            currentView.progressResources.visibility = View.VISIBLE
        }
    }

    /*fun isRefreshingData(isRefreshing: Boolean) {
        this.isRefreshing = isRefreshing
        if (isRefreshing) {
            toggleViews(true)
        } else {
            toggleViews()
        }
    }*/
}