package com.up.main.Fragments

import android.app.Activity
import android.content.Intent
import com.up.R
import com.up.constants.CAMERA
import com.up.constants.DATA_QR_RESULT
import com.up.constants.EVENT_QR_CODE
import com.up.constants.TYPE_DOWNLOAD
import com.up.main.BaseActivity
import com.up.main.DashboardActivity
import com.up.main.QRScanner
import com.up.utils.ConnectivityReceiver
import kotlinx.android.synthetic.main.fragment_receive.view.*
import org.jetbrains.anko.support.v4.toast


class FragmentReceive : BaseFragment() {

    lateinit var dashboardActivity: DashboardActivity
    override fun setContentView(): Int {
        return R.layout.fragment_receive
    }

    override fun init() {
        dashboardActivity = mContext as DashboardActivity
    }

    override fun buttonClicks() {

        currentView.btnReceive.setOnClickListener {
            receiveData()
        }

        currentView.btnBarcode.setOnClickListener {
            hideKeyboard()
            baseActivity.askPermission(mContext, object : BaseActivity.PermissionListener {
                override fun onGranted() {
                    startActivityForResult(Intent(mContext, QRScanner::class.java), EVENT_QR_CODE)
                }

                override fun onDenied() {
                }

            }, CAMERA)
        }

    }

    fun validateCode(code: String): Boolean {
        return (code.isNotEmpty() && code.trim().length >= 5)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {

                EVENT_QR_CODE -> {
                    currentView.etCode.setText(baseActivity.getFileName(data?.getStringExtra(DATA_QR_RESULT)!!))
                    receiveData()
                }
            }

        }
    }

    private fun receiveData() {
        baseActivity.hideKeyboard()

        if (!ConnectivityReceiver.isConnected()) {
            dashboardActivity.showSnackBar(mContext.resources.getString(R.string.no_internet))
        }

        val code = currentView.etCode.text.toString().trim()
        if (validateCode(code)) {
            (mContext as DashboardActivity).findTransfer(code, TYPE_DOWNLOAD)
        } else {
            toast("Please enter a valid code.")
        }
    }

    override fun onResume() {
        super.onResume()

        try {
            currentView.etCode.setText("")

            currentView.etCode.post {
                currentView.etCode.requestFocus()
                baseActivity.openKeyboard(currentView.etCode)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)

        if(menuVisible){
            currentView.etCode.setText("")

            currentView.etCode.post {
                currentView.etCode.requestFocus()
                baseActivity.openKeyboard(currentView.etCode)
            }
        }else{
            baseActivity.hideKeyboard()
        }
    }
}