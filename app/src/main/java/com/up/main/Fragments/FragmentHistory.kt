package com.up.main.Fragments

import android.arch.lifecycle.Observer
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.up.R
import com.up.adapter.HistoryAdapter
import com.up.constants.APP_SHARE_BASE_URL
import com.up.main.DashboardActivity
import kotlinx.android.synthetic.main.fragment_history.view.*
import org.jetbrains.anko.support.v4.alert

class FragmentHistory : BaseFragment() {

    lateinit var historyAdapter: HistoryAdapter
    lateinit var dashboardActivity: DashboardActivity

    var forceRefresh = false
    var downloadID = ""

    override fun setContentView(): Int {
        return R.layout.fragment_history
    }

    override fun init() {

        dashboardActivity = mContext as DashboardActivity

        historyAdapter = HistoryAdapter(mContext, this)
        currentView.recyclerHistory.layoutManager = LinearLayoutManager(mContext)
        currentView.recyclerHistory.adapter = historyAdapter

        historyViewModel.getAllDetail().observe(this, Observer {
            if (forceRefresh) {
                historyAdapter.submitList(null)
                forceRefresh = false
            }
            if (it != null && it.isNotEmpty()) {
                currentView.txtNoRecord.visibility = View.GONE
            } else {
                currentView.txtNoRecord.visibility = View.VISIBLE
            }
            historyAdapter.submitList(it)
        })

        historyViewModel.getAllRemainingDownloads().observe(this, Observer {
            if (it != null && it.isNotEmpty()) {
                downloadID = it[0].id!!
                dashboardActivity.initDownload(it[0])
            }
        })
    }

    override fun buttonClicks() {
    }

    override fun onResume() {
        super.onResume()

        try {
            historyAdapter.notifyDataSetChanged()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun showSharingPopup(code: String) {
        alert {
            title = "Up"

            message = "Do you want to share Link?"

            positiveButton("Yes") {
                historyAdapter.shareLink("$APP_SHARE_BASE_URL$code")
            }

            negativeButton("No") {
                it.dismiss()
            }

            show()
        }
    }
}