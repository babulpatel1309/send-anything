package com.up.main.Fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.up.R
import com.up.main.TestActivity
import kotlinx.android.synthetic.main.test_fragment_1.view.*

class TestFragment3 : Fragment() {

    lateinit var rootView: View
    lateinit var mContext: Context
    lateinit var testActivity: TestActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        highLight("Create")
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = context!!
        highLight("Attach")
    }

    override fun onStart() {
        super.onStart()
        highLight("Start")
    }

    override fun onAttachFragment(childFragment: Fragment?) {
        super.onAttachFragment(childFragment)
        highLight("AttachFragment")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        highLight("CreateView")
        rootView = inflater.inflate(R.layout.test_fragment_1, container, false)
        rootView.button.text = "Fragment 3"

        testActivity = activity as TestActivity

        rootView.button.setOnClickListener {
            val inBackStack = activity?.supportFragmentManager?.popBackStack(TestFragment3().javaClass.simpleName, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            highLight("Return $inBackStack")
//            testActivity.removeFragment(this)
        }

        return rootView
    }

    override fun onResume() {
        super.onResume()

        highLight("Resume")
    }

    override fun onPause() {
        super.onPause()
        highLight("Pause")
    }

    override fun onStop() {
        super.onStop()
        highLight("Stop")
    }

    override fun onDestroy() {
        super.onDestroy()
        highLight("Destroy")
    }

    override fun onDetach() {
        super.onDetach()
        highLight("Detach")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        highLight("DestroyView")
    }

    fun highLight(msg: String) {
        Log.e("Fragment3 : ", msg)
    }
}