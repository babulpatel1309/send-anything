package com.up.main

import android.app.Activity
import android.app.ActivityManager
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.arch.lifecycle.ProcessLifecycleOwner
import android.content.Context
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.database.Cursor
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.CountDownTimer
import android.os.Environment
import android.os.Handler
import android.provider.ContactsContract
import android.provider.MediaStore
import android.support.design.widget.BottomSheetDialog
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v7.widget.ShareActionProvider
import android.util.Log
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import bolts.AppLinks
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.crashlytics.android.Crashlytics
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.downloader.PRDownloaderConfig
import com.google.gson.JsonObject
import com.up.BuildConfig
import com.up.R
import com.up.constants.*
import com.up.data.Bean.CreateFileTransferBean
import com.up.data.Bean.FilesItem
import com.up.main.Fragments.*
import com.up.utils.*
import com.up.utils.Observable
import ir.mahdi.mzip.zip.ZipArchive
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.bottom_sheet_expire_time.view.*
import kotlinx.android.synthetic.main.custom_actionbar.view.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.delay
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.jetbrains.anko.alert
import org.jetbrains.anko.coroutines.experimental.bg
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


// Created by Babul Patel on 25/5/18.

class DashboardActivity : BaseActivity(), BaseActivity.AdapterItemSelectionListener, LifecycleObserver {


    lateinit var adapterItemSelectionListener: AdapterItemSelectionListener
    var shareActionProvider: ShareActionProvider? = null

    var scrollIdle = true
    var checkboxes = ArrayList<CheckBox>()
    var orderBy = MediaStore.Video.Media.DATE_TAKEN
    var orderByDateAdded = MediaStore.Video.Media.DATE_ADDED
    var orderByAudio = MediaStore.Audio.Media.ALBUM
    var orderByContacts = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME

    var retCol = arrayOf(MediaStore.Audio.Media._ID)

    var projectionImages = arrayOf(MediaStore.Images.Media.BUCKET_ID, MediaStore.Images.Media.DATA, MediaStore.Images.Media.DISPLAY_NAME, MediaStore.Images.Media.DATE_ADDED,
            MediaStore.Images.Media.SIZE, MediaStore.Images.Media.MIME_TYPE)

    var projectionVideos = arrayOf(MediaStore.Video.Media.BUCKET_ID, MediaStore.Video.Media.DATA, MediaStore.Video.Media.DISPLAY_NAME, MediaStore.Video.Media.DATE_ADDED,
            MediaStore.Video.Media.SIZE, MediaStore.Video.Media.MIME_TYPE)

    var projectionAudios = arrayOf(MediaStore.Audio.Media.ALBUM_ID, MediaStore.Audio.Media.DATA, MediaStore.Audio.Media.ALBUM, MediaStore.Audio.Media.DISPLAY_NAME,
            MediaStore.Audio.Media.SIZE, MediaStore.Audio.Media.MIME_TYPE, MediaStore.Audio.Media.DURATION, MediaStore.Audio.Media.DATE_ADDED)

    var projectionContacts = arrayOf(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.Contacts.PHOTO_THUMBNAIL_URI)

    var defaultExpiryTime = 24
    var defaultExpiryTimeText = "Expires in 1 day"

    override fun setContentView(): Int {
        return R.layout.activity_dashboard
    }

    override fun init() {
        initProcessObserver()

        setSupportActionBar(titleLay.toolbar)
        setVersionInfo()
        getDeepLinkingCode()
        initSnackbar()

        setToolbar("Dashboard", false, titleLay)
        adapterItemSelectionListener = this

        checkboxes.clear()

        checkboxes.add(chckPhotos)
        checkboxes.add(chckVideos)
        checkboxes.add(chckContacts)
        checkboxes.add(chckAudio)

        checkboxes.forEach {
            checkBoxClick(it)
        }

        chckPhotos.isChecked = true

        configureAdapter()
        fetchAllImages()

        initReceiver(true)

        EventBus.getDefault().register(this@DashboardActivity)
    }

    private fun setVersionInfo() {
        txtVersion.text = "v ${BuildConfig.VERSION_NAME}"
    }

    override fun buttonClicks() {
        bottomNavBar.itemIconTintList = null
        defaultSelector()

        btnSend.setOnClickListener {
            if (!ConnectivityReceiver.isConnected()) {
                toast(resources.getString(R.string.error_internet_down))
                return@setOnClickListener
            }

            createTransferReady()
        }

        btnMore.setOnClickListener {
            showBottomSheetDialog(object : ExpiryValidation {
                override fun onTimeSelected(time: Int) {
                    txtExpiry.post {
                        txtExpiry.text = defaultExpiryTimeText
                    }

                    defaultExpiryTime = time
                }
            })
        }
    }

    override fun onResume() {
        super.onResume()

        if (intent != null && intent.flags > 0) {
            onNewIntent(intent)
        }

        if (dataModelImages.isEmpty() && dataModelVideos.isEmpty()) {
            refreshFragments()
            highLight("Refreshed")
            fetchAllImages()
        }
    }

    override fun onPause() {
        super.onPause()
        triggered = false
    }

    private fun refreshFragments() {

        try {
            if (registeredFragments.size() <= 0) return

            when (pager.currentItem) {
                0 -> {
                    photosFragment = registeredFragments[0] as PhotosFragment
                }
                1 -> {
                    videosFragment = registeredFragments[1] as VideosFragment
                }
                2 -> {
                    contactsFragment = registeredFragments[2] as ContactsFragment
                }
                3 -> {
                    audioFragment = registeredFragments[3] as AudioFragment
                }
                4 -> {
                    appsFragment = registeredFragments[4] as AppsFragment
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
            highLight("Fragment refresh error.")
        }

    }

    lateinit var pagerAdapter: PagerAdapter

    fun configureAdapter() {

        /** configure viewpager*/

        pagerAdapter = PagerAdapter(context, supportFragmentManager)
        pager.adapter = pagerAdapter
        pager.offscreenPageLimit = 5

        tabsLay.setupWithViewPager(pager)
    }


    val sortedDates = ArrayList<String>()
    val sortedDatesFormat = ArrayList<String>()

    var dataModelImages = ArrayList<MainDataList>()
    var dataModelVideos = ArrayList<MainDataList>()
    var dataModelAudio = ArrayList<MainDataList>()
    var dataModelContacts = ArrayList<MainDataList>()
    var dataModelInstalledApps = ArrayList<MainDataList>()

    data class BasicAppInfo(var name: String, var size: Long, var icon: Drawable? = null, var apkFile: String? = null)

    data class MainDataList(var date: String = "", var data: Cursor? = null, var applicationInfo: BasicAppInfo? = null,
                            var icon: Drawable? = null, var iconListener: Observable<Drawable>? = null)

    var isOperationRunning = false

    fun fetchAllImages() {
        async(UI) {

            try {
                if (isOperationRunning) return@async

                isOperationRunning = true
                val sortedDatesImages = ArrayList<String>()
                val sortedDatesFormatImages = ArrayList<String>()
                dataModelImages.clear()

                try {
                    refreshGallery("file://" + Environment.getExternalStorageDirectory())
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                val result = bg {
                    val cursor = contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            null, null, null, "$orderByDateAdded DESC")

                    highLight("Images count ${cursor.count}")
                    cursor.moveToFirst()

                    for (i in 0 until cursor.count) {
                        cursor.moveToPosition(i)

                        try {
                            if (!sortedDatesFormatImages.contains(getDateFromTimeStamp(cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATE_ADDED)).toString()))
                                    && isFileExist(cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA)))
                            ) {
                                sortedDatesFormatImages.add(getDateFromTimeStamp(cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATE_ADDED)).toString()))
                                sortedDatesImages.add(cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATE_ADDED)))
                            }

                        } catch (e: Exception) {
                            highLight("Sorted dates format error")
                            e.printStackTrace()
                        }
                    }

                    highLight("Sorted count ${sortedDatesImages.size}")
                    dataModelImages.clear()

                    for (i in 0 until sortedDatesImages.size) {

                        try {
                            /*val scopeCursor = contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                    null, "${MediaStore.Images.Media.DATE_ADDED} >? and  ${MediaStore.Images.Media.DATE_ADDED} <? ",
                                    getRangeFromDateSeconds(sortedDatesImages[i].toLong()), "$orderBy DESC")*/

                            val scopeCursor = contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                    null,
                                    "(${MediaStore.Video.Media.DATE_TAKEN} >? and  ${MediaStore.Video.Media.DATE_TAKEN} <? ) or (${MediaStore.Video.Media.DATE_ADDED} >? and  ${MediaStore.Video.Media.DATE_ADDED} <? )",
                                    getRangeFromDateSeconds(sortedDatesImages[i].toLong()), "$orderBy DESC")

                            if (scopeCursor.count > 0)
                                dataModelImages.add(MainDataList(sortedDatesFormatImages[i], scopeCursor))
                        } catch (e: Exception) {
                            e.printStackTrace()
                            highLight("Sorted dates error")
                        }
                    }

                    cursor.close()
                }

                result.await()

                if (photosFragment.isAdded) {
                    photosFragment.onMimeTypeChange()
                } else {
                    highLight("Skipped Photos")
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Crashlytics.logException(e)
            }

            fetchAllVideos()
        }
    }

    fun fetchAllVideos(singleOperation: Boolean = false) {
        async(UI) {


            sortedDates.clear()
            sortedDatesFormat.clear()
            dataModelVideos.clear()
            val result = bg {

                val cursor = contentResolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                        null, null, null, "$orderBy DESC")

                try {
                    cursor!!.moveToFirst()

                    for (i in 0 until cursor.count) {
                        cursor.moveToPosition(i)

                        highLight("File Name : ${cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA))}/n File Added ${cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATE_ADDED))}")
                        try {
                            if (!sortedDatesFormat.contains(getDateFromTimeStamp(cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATE_ADDED)).toString()))
                                    && isFileExist(cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA)))) {
                                sortedDatesFormat.add(getDateFromTimeStamp(cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATE_ADDED)).toString()))
                                sortedDates.add(cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATE_ADDED)))
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    for (i in 0 until sortedDates.size) {

                        val scopeCursor = contentResolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                                null,
                                "(${MediaStore.Video.Media.DATE_TAKEN} >? and  ${MediaStore.Video.Media.DATE_TAKEN} <? ) or (${MediaStore.Video.Media.DATE_ADDED} >? and  ${MediaStore.Video.Media.DATE_ADDED} <? )",
                                getRangeFromDate(sortedDates[i].toLong()), "$orderBy DESC")

                        if (scopeCursor.count > 0)
                            dataModelVideos.add(MainDataList(sortedDatesFormat[i], scopeCursor))
                    }

                    cursor.close()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            result.await()
            if (videosFragment.isAdded) {
                videosFragment.onMimeTypeChange()
            }
            fetchAllAudios()
        }
    }

    fun fetchAllAudios() {
        async(UI) {

            sortedDates.clear()
            sortedDatesFormat.clear()
            dataModelAudio.clear()
            val result = bg {

                val cursor = contentResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                        projectionAudios, null, null, "$orderByAudio ASC")

                try {
                    cursor!!.moveToFirst()

                    for (i in 0 until cursor.count) {
                        cursor.moveToPosition(i)

                        try {

                            if (!sortedDatesFormat.contains(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM)))
                                    && isFileExist(cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA)))) {
                                sortedDatesFormat.add(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM)))
                                sortedDates.add(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM)))
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    for (i in 0 until sortedDates.size) {

                        val scopeCursor = contentResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                                projectionAudios, "${MediaStore.Audio.Media.ALBUM} = ?",
                                arrayOf(sortedDates[i]), "$orderByAudio DESC")


                        if (scopeCursor.count > 0)
                            dataModelAudio.add(MainDataList(sortedDatesFormat[i], scopeCursor))
                    }

                    cursor.close()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            result.await()
            isOperationRunning = false

            if (audioFragment.isAdded) {
                audioFragment.onMimeTypeChange()
            }
        }
    }

    fun fetchAllContacts(operationResponder: OperationResponder) {
        async(UI) {

            sortedDates.clear()
            sortedDatesFormat.clear()
            dataModelContacts.clear()
            val result = bg {

                val cursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null, null, null, "${ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME} ASC")

                try {
                    cursor!!.moveToFirst()

                    for (i in 0 until cursor.count) {
                        cursor.moveToPosition(i)

                        try {

                            if (!sortedDatesFormat.contains(getFirstLetter(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))))) {
                                sortedDatesFormat.add(getFirstLetter(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))))
                                sortedDates.add(getFirstLetter(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))))
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    for (i in 0 until sortedDates.size) {

                        val scopeCursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null, "${ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME} Like ?",
                                arrayOf("${sortedDates[i]}%"), "$orderByContacts ASC")


                        if (scopeCursor.count > 0)
                            dataModelContacts.add(MainDataList(sortedDatesFormat[i], scopeCursor))
                    }

                    cursor.close()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            result.await()

            operationResponder.OnComplete(dataModelContacts)
        }
    }

    fun fetchAllInstalledApps(operationResponder: OperationResponder) {

        async(UI) {

            val result = bg {

                val apps = packageManager.getInstalledApplications(0)

                apps.forEachIndexed { index, applicationInfo ->

                    when {
                        applicationInfo.flags and ApplicationInfo.FLAG_UPDATED_SYSTEM_APP !== 0 -> {

                            val size = File(applicationInfo.publicSourceDir)
                            val basicAppInfo = BasicAppInfo(applicationInfo.loadLabel(packageManager).toString(), size.length(), applicationInfo?.loadIcon(packageManager), applicationInfo?.publicSourceDir)
                            dataModelInstalledApps.add(MainDataList(date = "My Apps", applicationInfo = basicAppInfo))
                        }

                        //it's a system app, not interested
                        applicationInfo.flags and ApplicationInfo.FLAG_SYSTEM !== 0 -> {
                            //Discard this one
                            //in this case, it should be a user-installed app
                        }
                        else -> {
                            val size = File(applicationInfo.publicSourceDir)
                            val basicAppInfo = BasicAppInfo(applicationInfo.loadLabel(packageManager).toString(), size.length(), applicationInfo?.loadIcon(packageManager), applicationInfo?.publicSourceDir)
                            dataModelInstalledApps.add(MainDataList(date = "My Apps", applicationInfo = basicAppInfo))
                        }
                    }
                }

                Collections.sort(dataModelInstalledApps, AppsSorter())
            }
            result.await()

            if (appsFragment.isAdded) {
                appsFragment.onMimeTypeChange()
            }
        }
    }

    var photosFragment = PhotosFragment()
    var videosFragment = VideosFragment()
    var contactsFragment = ContactsFragment()
    var audioFragment = AudioFragment()
    var appsFragment = AppsFragment()

    companion object {
        val registeredFragments = SparseArray<Fragment>()
    }

    inner class PagerAdapter(var context: Context, fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val fragment = super.instantiateItem(container, position)
            registeredFragments.put(position, fragment as Fragment?)
            return fragment
        }

        // This determines the fragment for each tab
        override fun getItem(position: Int): Fragment? {
            return when (position) {
                0 -> photosFragment
                1 -> videosFragment
                2 -> contactsFragment
                3 -> audioFragment
                4 -> appsFragment
                else -> null
            }
        }

        // This determines the number of tabs
        override fun getCount(): Int {
            return 5
        }

        // This determines the title for each tab
        override fun getPageTitle(position: Int): CharSequence? {
            // Generate title based on item position
            return when (position) {
                0 -> context.getString(R.string.tab_photo)
                1 -> context.getString(R.string.tab_video)
                2 -> context.getString(R.string.tab_contacts)
                3 -> context.getString(R.string.tab_audio)
                4 -> context.getString(R.string.tab_apps)
                else -> null
            }
        }
    }

    override fun onItemSelected() {

        var count = 0

        if (photosFragment.stickyHeaderAdapter != null) {
            count += photosFragment.stickyHeaderAdapter?.selectedFiles?.size!!
        }

        if (videosFragment.stickyHeaderAdapter != null) {
            count += videosFragment.stickyHeaderAdapter?.selectedFiles?.size!!
        }

        if (contactsFragment.stickyHeaderAdapter != null) {
            count += contactsFragment.stickyHeaderAdapter?.selectedFiles?.size!!
        }

        if (audioFragment.stickyHeaderAdapter != null) {
            count += audioFragment.stickyHeaderAdapter?.selectedFiles?.size!!
        }

        if (appsFragment.stickyHeaderAdapter != null) {
            count += appsFragment.stickyHeaderAdapter?.selectedFiles?.size!!
        }

        if (count > 0) {
            txtExpiry.text = defaultExpiryTimeText
            selectionCard.visibility = View.VISIBLE
        } else selectionCard.visibility = View.GONE
        txtSelectionCount.text = "$count Selected"
        txtSelectionSize.text = getSizeInMB(totalSize)
    }

    fun checkBoxClick(checkBox: CheckBox) {
        checkBox.setOnClickListener {
            toggleCheckBox(it as CheckBox)
            when (it.id) {
                chckPhotos.id -> {
                    pager.currentItem = 0
                }
                chckVideos.id -> {
                    pager.currentItem = 1
                }
                chckContacts.id -> {
                    pager.currentItem = 2
                }
                chckAudio.id -> {
                    pager.currentItem = 3
                }
            }

        }
    }

    fun toggleCheckBox(checkBox: CheckBox) {
        checkboxes.forEach {
            it.isChecked = it.id == checkBox.id
        }
    }

    val fragmentReceive = FragmentReceive()
    val fragmentHistory = FragmentHistory()

    fun defaultSelector() {
        bottomNavBar.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navend -> {
//                    removeAllFragment()
                    container.visibility = View.GONE
                    return@setOnNavigationItemSelectedListener true
                }
                /*R.id.navReceive -> {
                    container.visibility = View.VISIBLE
                    val tabContainer = container.childCount
                    replaceFragment(fragmentReceive)
                    Handler().postDelayed({
                        if (tabContainer > 0)
                            fragmentReceive.onResume()
                    }, 500)

                    return@setOnNavigationItemSelectedListener true
                }*/

                R.id.navHistory -> {
                    container.visibility = View.VISIBLE
                    val tabContainer = container.childCount
                    replaceFragment(fragmentHistory)
                    Handler().postDelayed({
                        if (tabContainer > 0)
                            fragmentHistory.onResume()
                    }, 500)

                    return@setOnNavigationItemSelectedListener true
                }

                else -> {
                    return@setOnNavigationItemSelectedListener true
                }

            }
        }
    }

    data class LastSelectedModel(val mimeType: String, val path: String, val size: Long, val icon: Drawable? = null, val name: String = "")

    val listOfSelectedFiles = ArrayList<LastSelectedModel>()
    var listOfSelectedFilesPos = ArrayList<String>()
    var totalSize: Long = 0

    fun checkLastSelected(lastSelected: LastSelectedModel, add: Boolean, position: String) {

        try {
            val uniqueValue = "${lastSelected.mimeType}_$position"
            if (add && !listOfSelectedFilesPos.contains(uniqueValue)) {
                listOfSelectedFiles.add(lastSelected)
                listOfSelectedFilesPos.add(uniqueValue)

                if (lastSelected.mimeType != MT_CONTACT)
                    totalSize += lastSelected.size
            } else {
                listOfSelectedFiles.remove(lastSelected)
                listOfSelectedFilesPos.remove(uniqueValue)

                if (lastSelected.mimeType != MT_CONTACT)
                    totalSize -= lastSelected.size
            }

            async(UI) {
                try {
                    if (listOfSelectedFiles.size > 0) {

                        val lastCursor = listOfSelectedFiles[listOfSelectedFilesPos.size - 1]
                        val requestOptions = RequestOptions().override(150, 150)

                        when (lastCursor.mimeType) {

                            MT_JPEG -> {
                                Glide.with(this@DashboardActivity)
                                        .load(lastCursor.path)
                                        .apply(requestOptions)
                                        .into(imgLastSelected)
                            }

                            MT_VIDEO -> {
                                Glide.with(this@DashboardActivity)
                                        .load(lastCursor.path)
                                        .apply(requestOptions)
                                        .into(imgLastSelected)
                            }

                            MT_AUDIO -> {
                                Glide.with(this@DashboardActivity)
                                        .load(R.drawable.ic_audio)
                                        .apply(requestOptions)
                                        .into(imgLastSelected)
                            }

                            MT_CONTACT -> {
                                Glide.with(this@DashboardActivity)
                                        .load(R.drawable.ic_user_contact)
                                        .apply(requestOptions)
                                        .into(imgLastSelected)
                            }

                            MT_APPS -> {
                                Glide.with(this@DashboardActivity)
                                        .load(lastCursor.icon)
                                        .apply(requestOptions)
                                        .into(imgLastSelected)
                            }

                            else -> {

                            }

                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                onItemSelected()
            }

        } catch (e: Exception) {
            e.printStackTrace()

            onItemSelected()
        }

    }

    lateinit var customContentImages: ContentObserverImages
    lateinit var customContentImagesInternal: ContentObserverImages
    lateinit var customContentVideos: ContentObserverVideos

    fun initReceiver(register: Boolean) {

        if (register) {

            customContentImages = ContentObserverImages(context, object : OperationResponder {
                override fun OnComplete(returnValue: Any?) {
                    async(UI) {
                        fetchAllImages()
                    }
                }
            })

            customContentImagesInternal = ContentObserverImages(context, object : OperationResponder {
                override fun OnComplete(returnValue: Any?) {
                    async(UI) {
                        fetchAllImages()
                    }
                }
            })

            customContentVideos = ContentObserverVideos(context, object : OperationResponder {
                override fun OnComplete(returnValue: Any?) {
                    async(UI) {
                        fetchAllVideos()
                    }
                }
            })

            contentResolver.registerContentObserver(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    false, customContentImages)

            contentResolver.registerContentObserver(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                    false, customContentVideos)

            contentResolver.registerContentObserver(MediaStore.Images.Media.INTERNAL_CONTENT_URI,
                    false, customContentImagesInternal)
        } else {
            contentResolver.unregisterContentObserver(customContentImages)
            contentResolver.unregisterContentObserver(customContentImagesInternal)
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        initReceiver(false)
    }

    inner class AppsSorter : Comparator<MainDataList> {
        override fun compare(o1: MainDataList?, o2: MainDataList?): Int {
            return o1?.applicationInfo?.name?.compareTo(o2?.applicationInfo?.name!!)!!
        }
    }

    inner class DateSorter : Comparator<String> {
        override fun compare(o1: String, o2: String): Int {
            return o1.toLong().compareTo(o2.toLong())
        }
    }

    private fun createTransferReady() {

        showProgressDialog()

        var totalFileSize = 0L
        val arrFiles = ArrayList<JsonObject>()
        var contactAdded = false
        for (file in listOfSelectedFiles) {
            if (file.mimeType == MT_CONTACT && contactAdded) continue

            val jsonObject = JsonObject()
            when {
                file.mimeType == MT_APPS -> jsonObject.addProperty(PARAM_NAME, file.name)
                file.mimeType == MT_CONTACT -> jsonObject.addProperty(PARAM_NAME, CONTACT_VCF)
                else -> jsonObject.addProperty(PARAM_NAME, getFileName(file.path))
            }

            jsonObject.addProperty(PARAM_TYPE, file.mimeType)
            jsonObject.addProperty(PARAM_SIZE, file.size)

            arrFiles.add(jsonObject)

            if (file.mimeType == MT_CONTACT) contactAdded = true
            totalFileSize += file.size
        }

        val map = HashMap<String, Any>()
        map[PARAM_TYPE] = TYPE_LINK
        map[PARAM_TTL] = defaultExpiryTime
        map[PARAM_FILES] = arrFiles
        callWS()?.createTransfer(map)?.enqueue(object : Callback<CreateFileTransferBean> {
            override fun onFailure(call: Call<CreateFileTransferBean>, t: Throwable) {
                t.printStackTrace()
            }

            override fun onResponse(call: Call<CreateFileTransferBean>, response: Response<CreateFileTransferBean>) {
                if (isSuccess(response.code())) {
                    setDynamicCode(response.body()?.code.toString())
                    findTransfer(getDynamicCode(), TYPE_UPLOAD)
//                    showCode()
                } else {
                    toast("Something went wrong -- Error code : ${response.code()}")
                }

            }
        })
    }

    fun showCode() {
        goWithResult(CodeActivity::class.java, EVENT_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {

                EVENT_CODE -> {
                    findTransfer(getDynamicCode(), TYPE_UPLOAD)
                    uploadAttachment()
                }
            }

        }
    }


    var isUploading = false
    var activeUploadId: Int = 0
    var activeUploadItemId = ""
    var uploadingBean: CreateFileTransferBean? = null
    var callUpload: Call<ResponseBody>? = null

    private fun uploadAttachment() {

        if (isUploading) {
            return
        }

        isUploading = true

        val listOfMultipartBody = ArrayList<MultipartBody.Part>()
        var isContactAttached = false

        uploadingFilesCount = listOfSelectedFiles.size

        listOfSelectedFiles.forEachIndexed { index, lastSelectedModel ->
            if (lastSelectedModel.mimeType == MT_CONTACT) {
                if (!isContactAttached) {
                    listOfMultipartBody.add(prepareMultiPart(lastSelectedModel.name))
                    isContactAttached = true
                }
            } else {
                listOfMultipartBody.add(prepareMultiPart(lastSelectedModel.path))
            }
        }

        callUpload = callWS()?.uploadAttachments(ApiUrl.getInstance().getAPI(SEND_FILES), listOfMultipartBody)

        callUpload?.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                isUploading = false
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful && isSuccess(response.code())) {
                    uploadingFilesCount = 0
                    totalUploadingProgress = 0
                    isUploading = false
                    if (uploadingBean != null && uploadingBean?.downloaded == UPLOADING) {
                        uploadingBean?.downloaded = DOWNLOADED
                        fragmentHistory.forceRefresh = true
                        historyViewModel.addItem(uploadingBean)
                    }
                    toast("File sent.")
                } else {
                    Log.e("Error", response.errorBody().toString())
                }

                clearSelection()
            }
        })
    }

    var uploadingFilesCount = 0
    var totalUploadingProgress = 0
    fun prepareMultiPart(path: String): MultipartBody.Part {
        val sourceFile = File(path)
        val reqFile = ProgressRequestBody(sourceFile, uploadListener())
        val body = MultipartBody.Part.createFormData(PARAM_FILES, getFileName(path), reqFile)
        return body
    }

    fun uploadListener(): ProgressRequestBody.UploadCallbacks {

        return object : ProgressRequestBody.UploadCallbacks {
            override fun onProgressUpdate(percentage: Int) {
                Log.e("Uploading", percentage.toString())
                if (uploadingFilesCount == 1)
                    fireUploadProgressEvent(percentage)
                else {
                    countUploadProgress(percentage)
                }

            }

            override fun onError() {
            }

            override fun onFinish() {
            }

        }
    }

    var lastProgressCount = 0
    fun countUploadProgress(percentage: Int) {
        if (lastProgressCount != percentage) {
            Log.e("MultiFiles percentage", percentage.toString())
            lastProgressCount = percentage
            totalUploadingProgress++
            Log.e("MultiFilesProgress", totalUploadingProgress.toString())
            fireUploadProgressEvent(totalUploadingProgress / (uploadingFilesCount * 2))
        }
    }

    override fun onBackPressed() {
        if (bottomNavBar.selectedItemId == R.id.navend) {
            super.onBackPressed()
        } else {
            bottomNavBar.selectedItemId = R.id.navend
        }
    }

    fun findTransfer(code: String, transferType: Int) {

        Log.e("CODE", code)
        setDynamicCode(code)
        if (transferType == TYPE_DOWNLOAD) showProgressDialog()

        callWS()?.findTransfer(ApiUrl.getInstance().getAPI(FIND_TRANSFER))?.enqueue(object : Callback<CreateFileTransferBean> {
            override fun onFailure(call: retrofit2.Call<CreateFileTransferBean>, t: Throwable) {
                hideProgressDialog()
                t.printStackTrace()
                toast("Timeout. Please try again.")
            }

            override fun onResponse(call: retrofit2.Call<CreateFileTransferBean>, response: retrofit2.Response<CreateFileTransferBean>) {
                Log.e("Verbose", "File transfer created with code- ${response.code()}")
                if (response.isSuccessful && isSuccess(response.code())) {
                    //Download attachment

                    val createFileTransferBean = response.body()
                    createFileTransferBean?.transferType = transferType
                    when (transferType) {
                        TYPE_DOWNLOAD -> {
                            hideProgressDialog()
                            createFileTransferBean?.timestamp = getCurrentTimeStamp()
                            startDownload(createFileTransferBean)
                        }

                        TYPE_UPLOAD -> {
                            hideProgressDialog()
                            createFileTransferBean?.files = createUploadFileList()
                            createFileTransferBean?.timestamp = getCurrentTimeStamp()
                            createFileTransferBean?.downloaded = UPLOADING

                            activeUploadItemId = createFileTransferBean?.id!!

                            historyViewModel.addItem(createFileTransferBean, object : OperationResponder {
                                override fun OnComplete(returnValue: Any?) {
                                    uploadingBean = createFileTransferBean
                                    uploadingBean?.localID = (returnValue as Long).toInt()

                                    bottomNavBar.selectedItemId = R.id.navHistory
                                    uploadAttachment()

                                    async(UI) {
                                        delay(300)
                                        fragmentHistory.showSharingPopup(code)
                                    }
                                }
                            })

                        }
                    }

                } else {
                    toast("Link not found")
                    hideProgressDialog()
                }
            }
        })
    }

    fun startDownload(createFileTransferBean: CreateFileTransferBean?) {

        askPermission(context, object : PermissionListener {
            override fun onGranted() {

                if (!ConnectivityReceiver.isConnected()) {
                    showSnackBar(context.resources.getString(R.string.no_internet))
                    return
                }

                bottomNavBar.selectedItemId = R.id.navHistory
                createFileTransferBean?.downloaded = REMAINING

                async(UI) {
                    delay(500)
                    historyViewModel.addItem(createFileTransferBean)
                }
            }

            override fun onDenied() {
            }

        }, STORAGE)

    }

    var isDownloading = false
    var activeDownloadId: Int = 0
    var activeDownloadItemId = ""

    fun initDownload(createFileTransferBean: CreateFileTransferBean?) {

        if (isDownloading) return
        isDownloading = true

        createFileTransferBean?.let {

            val transferCode = getTransferCode(it)

            it.downloaded = DOWNLOADING
            it.code = transferCode
            historyViewModel.addItem(it)

            val filename = if (it.files != null &&
                    it.files?.size!! < 2) {

                it.files!![0]!!.name!!
            } else {
                "$transferCode.zip"
            }

            activeDownloadItemId = it.id!!

            isTransferActive(transferCode, object : DownloadListener {
                override fun onCompleted() {
                    receiveFiles(transferCode, filename, object : DownloadListener {
                        override fun onCompleted() {
                            it.downloaded = DOWNLOADED
                            historyViewModel.addItem(it)

                            isDownloading = false
                        }

                        override fun onError() {
                            it.downloaded = FAILED
                            historyViewModel.addItem(it)

                            isDownloading = false
                        }
                    })
                }

                override fun onError() {
                    Log.e("FAILED ---> ", transferCode)
                    it.downloaded = FAILED
                    historyViewModel.addItem(it)
                }
            })
        }
    }

    interface DownloadListener {
        fun onCompleted()
        fun onError()
    }

    fun receiveFiles(code: String, filename: String, downloadListener: DownloadListener) {

        Log.e("DOWNLOADING ---> ", code)

        val config = PRDownloaderConfig.newBuilder()
                .setConnectTimeout(60000)
                .setReadTimeout(60000)
                .build()

        PRDownloader.initialize(context, config)

        createDirectory(code)
        activeDownloadId = PRDownloader.download(ApiUrl.getInstance().getAPI(RECEIVE_FILES, code), "$docPath$code/", filename)
                .build()
                .setOnProgressListener {
                    val progress = (it.currentBytes * 100) / it.totalBytes
                    Log.e("Progress", ("$progress"))
                    fireDownloadProgressEvent(progress.toInt())
                }
                .start(object : OnDownloadListener {
                    override fun onDownloadComplete() {

                        if (getFileType(filename) == ".zip") {
                            async(UI) {
                                createDirectory(code)
                                val result = bg {
                                    ZipArchive.unzip("$docPath$code/$filename", "$docPath$code", "")
                                }

                                result.await()
                                toast("File received")
                                downloadListener.onCompleted()
                                delay(500)
                                scanFolder(object : onScanComplete {
                                    override fun onScanComplete() {
                                        Handler().postDelayed({
                                            fetchAllImages()
                                        }, 500)
                                    }
                                })
                            }
                        } else {
                            toast("File received")
                            downloadListener.onCompleted()
                            scanFolder()
                        }
                    }

                    override fun onError(error: Error?) {
                        toast("${error?.toString()} error")
                        downloadListener.onError()
                        scanFolder()
                    }
                })
    }

    interface onScanComplete {
        fun onScanComplete()
    }

    fun scanFolder(scanCompleteListener: onScanComplete? = null) {

        val folder = File(docPath)
        folder.listFiles()?.forEachIndexed { outeIindex, it ->
            if (it.isFile) {
                refreshGallery(it.absolutePath)
            } else if (it.isDirectory) {
                File(it.absolutePath).listFiles().forEachIndexed { index, file ->
                    refreshGallery(file.absolutePath)
                }
            }

            if (outeIindex >= (folder.listFiles().size - 1)) scanCompleteListener?.onScanComplete()
        }
    }

    var lastUpdate = 0L
    var updateThreshold = 500

    fun fireDownloadProgressEvent(progress: Int) {

        val timeStamp = System.currentTimeMillis()
        if ((timeStamp - lastUpdate) > updateThreshold) {
            val downloadUpdate = DownloadUpdate(activeDownloadItemId, progress, activeDownloadId, TYPE_DOWNLOAD)
            EventBus.getDefault().post(downloadUpdate)
            lastUpdate = timeStamp
        }
    }

    fun fireUploadProgressEvent(progress: Int) {
        val uploadUpdate = DownloadUpdate(activeUploadItemId, progress + 4, -1, TYPE_UPLOAD)
        EventBus.getDefault().post(uploadUpdate)
    }


    fun isTransferActive(code: String, downloadListener: DownloadListener) {

        callWS()?.findTransfer(ApiUrl.getInstance().getAPI(FIND_TRANSFER, code))?.enqueue(object : Callback<CreateFileTransferBean> {
            override fun onFailure(call: retrofit2.Call<CreateFileTransferBean>, t: Throwable) {
                t.printStackTrace()
                downloadListener.onError()
            }

            override fun onResponse(call: retrofit2.Call<CreateFileTransferBean>, response: retrofit2.Response<CreateFileTransferBean>) {
                Log.e("Verbose", "File transfer created with code- ${response.code()}")
                if (response.isSuccessful && isSuccess(response.code())) {
                    downloadListener.onCompleted()
                }
            }
        })
    }

    fun getTransferCode(createFileTransferBean: CreateFileTransferBean): String {
        var transferCode = createFileTransferBean.code.toString()

        if (transferCode.trim().length <= 5) {
            transferCode = "0$transferCode"
        }

        return transferCode
    }

    /**
     *Snackbar dept
     * */
    lateinit var snackbar: Snackbar

    fun initSnackbar() {
        snackbar = Snackbar.make(rootDashboard, "", Snackbar.LENGTH_SHORT)
    }

    fun showSnackBar(msg: String) {
        if (!snackbar.isShown) {
            snackbar.setText(msg)
            snackbar.show()
        }
    }

    fun hideSnackbar() {
        if (snackbar.isShown) snackbar.dismiss()
    }

    fun createUploadFileList(): ArrayList<FilesItem> {

        val listOfUploadingFiles = ArrayList<FilesItem>()
        listOfSelectedFiles.forEach {
            val filesItem = FilesItem()
            filesItem.name = it.path
            filesItem.size = it.size
            listOfUploadingFiles.add(filesItem)
        }

        return listOfUploadingFiles
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this@DashboardActivity)
    }

    /**
     * Events
     * */

    @Subscribe
    fun cancelDownload(downloadCancel: DownloadCancel) {
        if (downloadCancel.cancelType == CANCEL_DOWNLOAD) {

            downloadCancel.downloadId?.let {
                PRDownloader.cancel(it)
                Log.e("Download", "Cancelled")
            }
        } else if (downloadCancel.cancelType == CANCEL_UPLOAD) {
            callUpload?.cancel()
        }
    }

    fun clearSelection() {
        defaultExpiryTime = 24
        totalSize = 0
        listOfSelectedFiles.clear()
        listOfSelectedFilesPos.clear()

        try {
            async(UI) {
                photosFragment.stickyHeaderAdapter?.selectedFiles?.clear()
                videosFragment.stickyHeaderAdapter?.selectedFiles?.clear()
                audioFragment.stickyHeaderAdapter?.selectedFiles?.clear()
                appsFragment.stickyHeaderAdapter?.selectedFiles?.clear()
                contactsFragment.stickyHeaderAdapter?.selectedFiles?.clear()

                photosFragment.stickyHeaderAdapter?.notifyDataSetChanged()
                videosFragment.stickyHeaderAdapter?.notifyDataSetChanged()
                audioFragment.stickyHeaderAdapter?.notifyDataSetChanged()
                appsFragment.stickyHeaderAdapter?.notifyDataSetChanged()
                contactsFragment.stickyHeaderAdapter?.notifyDataSetChanged()

                onItemSelected()
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        getDeepLinkingCode()
    }

    var mIsRestoredToTop = false
    var triggered = false
    fun getDeepLinkingCode() {
        if (triggered) return
        triggered = true
        var data = intent.data
        if (data != null) {

            if (getTasksInfo() > 1 && !intent.getBooleanExtra(FROM_DEEP_LINKING, false)) {
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY)
                intent.putExtra(FROM_DEEP_LINKING, true)
                startActivity(intent)
                mIsRestoredToTop = true
                finish()
                return
            }

            if (!ConnectivityReceiver.isConnected()) {
                showSnackBar(resources.getString(R.string.no_internet))
                return
            }
            if (data.toString().contains("target")) {
                data = AppLinks.getTargetUrlFromInboundIntent(context, intent)
            }

            Log.e("Deep Link",data.toString())
            showDownloadConfirmation(data)
        }
    }

    fun getTasksInfo(): Int {
        val taskManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        return taskManager.appTasks.size
    }

    override fun finish() {
        super.finish()
        if (mIsRestoredToTop) {
            val taskManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            if (taskManager.appTasks.size > 1) {
                taskManager.appTasks[0].moveToFront()
                taskManager.appTasks[1].finishAndRemoveTask()
            }
        }
    }


    fun showDownloadConfirmation(data: Uri?) {
        alert {
            title = "Up"

            message = "Do you want to receive files?"

            positiveButton("Yes") {
                val code = data.toString().substring(data.toString().lastIndexOf("/") + 1)
                Log.e("Incoming data", code)
                findTransfer(code, TYPE_DOWNLOAD)
            }

            negativeButton("No") {
                it.dismiss()
            }

            show()
        }
    }

    interface ExpiryValidation {
        fun onTimeSelected(time: Int)
    }

    fun showBottomSheetDialog(expiryValidation: ExpiryValidation) {

        val bottomSheetDialog = BottomSheetDialog(context)
        val bottomSheetView = LayoutInflater.from(context).inflate(R.layout.bottom_sheet_expire_time, null)
        bottomSheetDialog.setContentView(bottomSheetView)

        bottomSheetView.btn1Hour.setOnClickListener {
            expiryValidation.onTimeSelected(1)
            defaultExpiryTimeText = "${resources.getString(R.string.expires_in)} ${resources.getString(R.string.expire_1_hour)}"
            bottomSheetDialog.dismiss()
        }

        bottomSheetView.btn4Hour.setOnClickListener {
            expiryValidation.onTimeSelected(4)
            defaultExpiryTimeText = "${resources.getString(R.string.expires_in)} ${resources.getString(R.string.expire_4_hour)}"
            bottomSheetDialog.dismiss()
        }

        bottomSheetView.btn12Hour.setOnClickListener {
            expiryValidation.onTimeSelected(12)
            defaultExpiryTimeText = "${resources.getString(R.string.expires_in)} ${resources.getString(R.string.expire_12_hour)}"
            bottomSheetDialog.dismiss()
        }

        bottomSheetView.btn1Day.setOnClickListener {
            expiryValidation.onTimeSelected(24)
            defaultExpiryTimeText = "${resources.getString(R.string.expires_in)} ${resources.getString(R.string.expire_1_day)}"
            bottomSheetDialog.dismiss()
        }

        bottomSheetView.btn3Day.setOnClickListener {
            expiryValidation.onTimeSelected(72)
            defaultExpiryTimeText = "${resources.getString(R.string.expires_in)} ${resources.getString(R.string.expire_3_day)}"
            bottomSheetDialog.dismiss()
        }

        bottomSheetDialog.show()
    }


    fun initProcessObserver() {
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppToForeground() {
        highLight("Counter stopped")
        stopCounter()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppToBackground() {
        highLight("Counter started")
        startCounter()
    }

    var countDownTimer: CountDownTimer? = null
    var deepSleepThreshold = 600000L

    fun startCounter() {

        if (countDownTimer == null) {
            countDownTimer = object : CountDownTimer(deepSleepThreshold, 1000) {
                override fun onFinish() {
                    highLight("Entering into deep sleep")
                    finishAffinity()
                }

                override fun onTick(p0: Long) {
                    highLight("$p0")
                }
            }.start()
        }
    }

    fun stopCounter() {
        if (countDownTimer != null) {
            countDownTimer?.cancel()
            countDownTimer = null
        }
    }

}