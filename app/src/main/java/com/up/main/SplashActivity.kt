package com.up.main

import com.up.R
import com.up.constants.PREF_FIRST_LAUNCH
import com.up.constants.SPLASH_TIMEOUT
import com.up.constants.STORAGE
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.delay
import android.provider.SyncStateContract.Helpers.update
import android.content.pm.PackageManager
import android.content.pm.PackageInfo
import android.util.Base64
import android.util.Log
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


// Created by Babul Patel on 5/5/18.

class SplashActivity : BaseActivity() {
    override fun setContentView(): Int {
        return R.layout.activity_splash
    }

    override fun init() {
        printHashKey()
        askPermission(context, object : PermissionListener {
            override fun onGranted() {
                if (prefs.getBooleanDetailTRUE(PREF_FIRST_LAUNCH)) {
                    prefs.setBooleanDetail(PREF_FIRST_LAUNCH, false)
                }

                async(UI) {
                    delay(SPLASH_TIMEOUT)
                    goWithFinish(DashboardActivity::class.java)
                }
            }

            override fun onDenied() {
            }

        }, STORAGE)

    }

    override fun buttonClicks() {
    }

    fun printHashKey() {
        try {
            val info = packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey = String(Base64.encode(md.digest(), 0))
                highLight("printHashKey() Hash Key: $hashKey")
            }
        } catch (e: NoSuchAlgorithmException) {
            Log.e(TAG, "printHashKey()", e)
        } catch (e: Exception) {
            Log.e(TAG, "printHashKey()", e)
        }

    }
}