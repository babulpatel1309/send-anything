package com.up.main

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.up.R
import com.up.main.Fragments.TestFragment1
import kotlinx.android.synthetic.main.test_activity.*

class TestActivity : AppCompatActivity(), FragmentManager.OnBackStackChangedListener {
    override fun onBackStackChanged() {
        highLight("Fragment Changed")
    }

    override fun onStart() {
        super.onStart()
        highLight("Start")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        highLight("Create")
        setContentView(R.layout.test_activity)
        init()
    }

    fun init() {

        val testFragment1 = TestFragment1()
        addFragment(testFragment1)
    }

    override fun onPause() {
        super.onPause()
        highLight("Pause")
    }

    override fun onResume() {
        super.onResume()

        highLight("Resume")
    }

    override fun onStop() {
        super.onStop()
        highLight("Stop")
    }

    override fun onDestroy() {
        super.onDestroy()
        highLight("Destroy")
    }

    fun highLight(msg: String) {
        Log.e("Activity : ", msg)
    }

    fun addFragment(fragment: Fragment) {

        supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(R.animator.slide_fragment_horizontal_right_in, R.animator.slide_fragment_horizontal_left_out, R.animator.slide_fragment_horizontal_left_in, R.animator.slide_fragment_horizontal_right_out)
                .add(container.id, fragment, fragment.javaClass.simpleName)
                .addToBackStack(fragment.javaClass.simpleName)
                .commit()

    }

    fun removeFragment(newFragment: Fragment) {
        supportFragmentManager
                .beginTransaction()
                .remove(newFragment)
                .commit()

    }

}