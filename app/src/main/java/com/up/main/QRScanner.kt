package com.up.main

import android.app.Activity
import android.content.Intent
import android.graphics.PointF
import com.dlazaro66.qrcodereaderview.QRCodeReaderView
import com.up.R
import com.up.constants.DATA_QR_RESULT
import kotlinx.android.synthetic.main.activity_qrreader.*

class QRScanner : BaseActivity(), QRCodeReaderView.OnQRCodeReadListener {
    override fun setContentView(): Int {
        return R.layout.activity_qrreader
    }

    override fun init() {

        qrdecoderview.setOnQRCodeReadListener(this)
        qrdecoderview.setAutofocusInterval(1000)
    }

    override fun buttonClicks() {
    }

    override fun onResume() {
        super.onResume()
        qrdecoderview.startCamera()
    }

    override fun onPause() {
        super.onPause()
        qrdecoderview.stopCamera()
    }

    override fun onQRCodeRead(text: String?, points: Array<out PointF>?) {
        val intent = Intent()
        intent.putExtra(DATA_QR_RESULT, text)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

}