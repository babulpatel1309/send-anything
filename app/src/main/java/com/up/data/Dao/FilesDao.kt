package com.up.data.Dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.up.data.Bean.FilesBean

@Dao
interface FilesDao {

    @Query("Select * from Filesdetail order by id ")
    fun getAllDetails(): LiveData<List<FilesBean>>

    @Query("Select * from Filesdetail where id=:id order by id ")
    fun getAllDetails(id: Int): List<FilesBean>

    @Query("Select * from Filesdetail where id=:id order by id ")
    fun getAllDetailsFiltered(id: Int): LiveData<List<FilesBean>>

    @Query("Select * from Filesdetail where mime_type= :mimeType order by date_added desc ")
    fun getAllDetailsFiltered(mimeType: String): LiveData<List<FilesBean>>

    @Query("Select * from Filesdetail where id=:uniqueID LIMIT 1")
    fun checkRecord(uniqueID: Int): List<FilesBean>

    @Query("Select * from Filesdetail where id=:uniqueID LIMIT 1")
    fun getRecord(uniqueID: Int): FilesBean

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addItems(transaction: List<FilesBean>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addItem(transaction: FilesBean): Long

    @Update
    fun updateItem(wsQuestionsBean: FilesBean)

    @Update
    fun updateItem(wsQuestionsBean: List<FilesBean>)

}