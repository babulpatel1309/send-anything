package com.up.data.Dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.up.data.Bean.CreateFileTransferBean

@Dao
interface HistoryFilesDao {

    @Query("Select * from History where itemExpired=0 order by localID desc")
    fun getAllDetails(): LiveData<List<CreateFileTransferBean>>

    @Query("Select * from History where downloaded=-1 and transferType=0 order by localID desc")
    fun getAllRemainingDownloads(): LiveData<List<CreateFileTransferBean>>

    @Query("Select * from History where id=:id order by id ")
    fun getAllDetails(id: Int): List<CreateFileTransferBean>

    @Query("Select * from History where id=:id order by id ")
    fun getAllDetailsFiltered(id: String): LiveData<List<CreateFileTransferBean>>

    @Query("Select * from History where id=:uniqueID LIMIT 1")
    fun checkRecord(uniqueID: String): List<CreateFileTransferBean>

    @Query("Select * from History where id=:uniqueID LIMIT 1")
    fun getRecord(uniqueID: Int): CreateFileTransferBean

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addItems(transaction: List<CreateFileTransferBean>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addItem(transaction: CreateFileTransferBean?): Long

    @Update
    fun updateItem(wsQuestionsBean: CreateFileTransferBean?)

    @Update
    fun updateItem(wsQuestionsBean: List<CreateFileTransferBean>)

}