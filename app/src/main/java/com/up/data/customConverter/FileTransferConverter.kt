package com.up.data.customConverter

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.up.data.Bean.FilesItem

class FileTransferConverter {
    val gson = Gson()

    @TypeConverter
    fun stringToSettingObject(data: String?): List<FilesItem> {
        if (data == null) {
            return listOf(FilesItem())
        }

        val listType = object : TypeToken<List<FilesItem>>() {

        }.type

        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun settingObjectToString(someObjects: List<FilesItem>): String {
        return gson.toJson(someObjects)
    }
}