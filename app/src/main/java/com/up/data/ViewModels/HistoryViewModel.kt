package com.up.data.ViewModels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import com.up.ApplicationClass
import com.up.data.Bean.CreateFileTransferBean
import com.up.data.Database.RoomDBHelper
import com.up.utils.OperationResponder
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class HistoryViewModel : ViewModel() {

    var transactionList: LiveData<List<CreateFileTransferBean>>
    var roomDBHelper: RoomDBHelper

    lateinit var config: PagedList.Config

    init {
        roomDBHelper = RoomDBHelper.getInstance(ApplicationClass.mInstance)
        transactionList = roomDBHelper.historyDao().getAllDetails()

        config = PagedList.Config.Builder()
                .setPageSize(500)
                .setEnablePlaceholders(false)
                .setInitialLoadSizeHint(500)
                .build()


    }

    fun getAllDetail(): LiveData<List<CreateFileTransferBean>> {
        return transactionList
    }

    fun getAllRemainingDownloads(): LiveData<List<CreateFileTransferBean>> {
        return roomDBHelper.historyDao().getAllRemainingDownloads()
    }

    fun getAllDetail(catID: String): LiveData<List<CreateFileTransferBean>> {
        return roomDBHelper.historyDao().getAllDetailsFiltered(catID)
    }

    fun getAllDetailsFiltered(mimeType: String): LiveData<List<CreateFileTransferBean>> {
        return roomDBHelper.historyDao().getAllDetailsFiltered(mimeType)
    }

    fun getAllQuestions(catID: Int, operationResponder: OperationResponder? = null) {
        async(UI) {
            val result = bg {
                return@bg roomDBHelper.historyDao().getAllDetails(catID)
            }
            operationResponder?.OnComplete(result.await())
        }
    }

    fun getTransaction(uniqueID: Int, operationResponder: OperationResponder? = null) {
        async(UI) {
            bg {
                operationResponder?.OnComplete(roomDBHelper.historyDao().getRecord(uniqueID))
            }
        }
    }

    fun updateItem(wsQuestionsBean: CreateFileTransferBean?, operationResponder: OperationResponder? = null) {
        async(UI) {
            bg {
                operationResponder?.OnComplete(roomDBHelper.historyDao().updateItem(wsQuestionsBean))
            }
        }
    }

    fun updateItems(wsQuestionsBean: List<CreateFileTransferBean>, operationResponder: OperationResponder? = null) {
        async(UI) {
            val result = bg {
                return@bg roomDBHelper.historyDao().updateItem(wsQuestionsBean)
            }
            operationResponder?.OnComplete(result.await())
        }
    }

    fun addItems(transaction: List<CreateFileTransferBean>, operationResponder: OperationResponder? = null) {
        async(UI) {
            val result = bg {

                transaction.forEach {
                    val record = roomDBHelper.historyDao().checkRecord(it.id!!)
                    if (record.isEmpty())
                        roomDBHelper.historyDao().addItem(it)
                }
            }

            operationResponder?.OnComplete(result.await())
        }
    }

    fun addItem(createFileTransferBean: CreateFileTransferBean?, operationResponder: OperationResponder? = null) {
        async(UI) {
            val result = bg {
                return@bg roomDBHelper.historyDao().addItem(createFileTransferBean)
            }

            operationResponder?.OnComplete(result.await())
        }
    }
}