package com.up.data.ViewModels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import com.up.ApplicationClass
import com.up.data.Bean.FilesBean
import com.up.data.Database.RoomDBHelper
import com.up.utils.OperationResponder
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class FilesViewModel : ViewModel() {

    var transactionList: LiveData<List<FilesBean>>
    var roomDBHelper: RoomDBHelper

    lateinit var config: PagedList.Config

    init {
        roomDBHelper = RoomDBHelper.getInstance(ApplicationClass.mInstance)
        transactionList = roomDBHelper.filesDao().getAllDetails()

        config = PagedList.Config.Builder()
                .setPageSize(500)
                .setEnablePlaceholders(false)
                .setInitialLoadSizeHint(500)
                .build()


    }

    fun getAllDetail(): LiveData<List<FilesBean>> {
        return transactionList
    }

    fun getAllDetail(catID: Int): LiveData<List<FilesBean>> {
        return roomDBHelper.filesDao().getAllDetailsFiltered(catID)
    }

    fun getAllDetailsFiltered(mimeType: String): LiveData<List<FilesBean>> {
        return roomDBHelper.filesDao().getAllDetailsFiltered(mimeType)
    }

    fun getAllQuestions(catID: Int, operationResponder: OperationResponder? = null) {
        async(UI) {
            val result = bg {
                return@bg roomDBHelper.filesDao().getAllDetails(catID)
            }
            operationResponder?.OnComplete(result.await())
        }
    }

    fun getTransaction(uniqueID: Int, operationResponder: OperationResponder? = null) {
        async(UI) {
            bg {
                operationResponder?.OnComplete(roomDBHelper.filesDao().getRecord(uniqueID))
            }
        }
    }

    fun updateQuestions(wsQuestionsBean: FilesBean, operationResponder: OperationResponder? = null) {
        async(UI) {
            bg {
                operationResponder?.OnComplete(roomDBHelper.filesDao().updateItem(wsQuestionsBean))
            }
        }
    }

    fun updateQuestions(wsQuestionsBean: List<FilesBean>, operationResponder: OperationResponder? = null) {
        async(UI) {
            val result = bg {
                return@bg roomDBHelper.filesDao().updateItem(wsQuestionsBean)
            }
            operationResponder?.OnComplete(result.await())
        }
    }

    fun addItems(transaction: List<FilesBean>,operationResponder: OperationResponder?=null) {
        async(UI) {
           val result =  bg {

                transaction.forEach {
                    val record = roomDBHelper.filesDao().checkRecord(it.id!!)
                    if (record.isEmpty())
                        roomDBHelper.filesDao().addItem(it)
                }
            }

            operationResponder?.OnComplete(result.await())
        }
    }


}