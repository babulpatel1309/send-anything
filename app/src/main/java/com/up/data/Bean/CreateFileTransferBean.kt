package com.up.data.Bean

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.up.data.customConverter.FileTransferConverter

@Entity(tableName = "History")
data class CreateFileTransferBean(

        @field:SerializedName("code")
        var code: String? = null,

        @field:SerializedName("address")
        var address: String? = null,

        @field:SerializedName("account_id")
        var accountId: String? = null,

        @field:SerializedName("size")
        var size: Long? = null,

        @field:SerializedName("updated_at")
        var updatedAt: String? = null,

        @TypeConverters(FileTransferConverter::class)
        @field:SerializedName("files")
        var files: List<FilesItem?>? = null,

        @field:SerializedName("created_at")
        var createdAt: String? = null,

        @field:SerializedName("id")
        var id: String? = null,

        @field:SerializedName("type")
        var type: String? = null,

        @field:SerializedName("ttl")
        var ttl: String? = null,

        @field:SerializedName("status")
        var status: String? = null,

        var downloaded: Int = 0, //-1 Remaining 0 Downloaded 1 Downloading 2 Failed 3 Uploading

        var transferType: Int = 0, //0 Download 1 Upload

        var timestamp: Long=0, //Created timestamp

        var itemExpired : Int = 0, //0 - Not deleted 1 - Deleted

        @PrimaryKey
        var localID: Int? = null
)