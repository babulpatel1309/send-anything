package com.up.data.Bean

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "FilesDetail")
data class FilesBean(
        @PrimaryKey(autoGenerate = true)
        var id: Int? = null,
        var name: String = "",
        var path: String = "",
        var date_added: Long = 0,
        var mime_type: String = "",
        var alreadySorted: Boolean = false,
        var file_size: Long = 0,
        var thumb_data: String? = null
)