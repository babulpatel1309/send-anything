package com.up.data.Bean

import com.google.gson.annotations.SerializedName

data class FilesItem(

	@field:SerializedName("size")
	var size: Long? = null,

	@field:SerializedName("name")
	var name: String? = null,

	@field:SerializedName("type")
	var type: String? = null
)