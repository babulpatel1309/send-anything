package com.up.data.Database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import android.util.Log
import com.up.constants.DATABASE_NAME
import com.up.data.Bean.CreateFileTransferBean
import com.up.data.Bean.FilesBean
import com.up.data.Dao.FilesDao
import com.up.data.Dao.HistoryFilesDao
import com.up.data.customConverter.FileTransferConverter
import java.io.FileOutputStream
import java.io.IOException


/**
 * Created by sotsys-055 on 8/2/18.
 */


@TypeConverters(FileTransferConverter::class)
@Database(entities = arrayOf(FilesBean::class, CreateFileTransferBean::class), version = 3, exportSchema = false)
abstract class RoomDBHelper : RoomDatabase() {

    abstract fun filesDao(): FilesDao
    abstract fun historyDao(): HistoryFilesDao

    companion object {

        lateinit var sInstance: RoomDBHelper
        var DB_PATH = ""

        fun getInstance(context: Context): RoomDBHelper {
//            return copyAttachedDatabase(context)
            return roomInstance(context)
        }

        fun copyAttachedDatabase(context: Context): RoomDBHelper {
            val dbPath = context.getDatabasePath(DATABASE_NAME)

            // If the database already exists, return
            if (dbPath.exists()) {
                return roomInstance(context)
            }

            // Make sure we have a path to the file
            dbPath.parentFile.mkdirs()

            // Try to copy database file
            try {
                val inputStream = context.assets.open(DATABASE_NAME)
                val output = FileOutputStream(dbPath)

                val buffer = ByteArray(1024)
                var length: Int = 0

                while ({ length = inputStream.read(buffer, 0, 1024);length }() > 0) {
                    output.write(buffer, 0, length)
                }

                output.flush()
                output.close()
                inputStream.close()
            } catch (e: IOException) {
                Log.d("NO", "Failed to open file", e)
                e.printStackTrace()
            }

            return roomInstance(context)
        }

        fun roomInstance(context: Context): RoomDBHelper {
            try {
                if (sInstance != null) {
                    return sInstance
                } else {
                    return sInstance //Patch
                }
            } catch (e: Exception) {
                Log.d("DB", "DB initialized")
                sInstance = Room.databaseBuilder(context, RoomDBHelper::class.java, DATABASE_NAME)
                        .fallbackToDestructiveMigration()
                        .build()

                return sInstance
            }
        }
    }


}