package com.up.adapter

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.codewaves.stickyheadergrid.StickyHeaderGridAdapter
import com.up.R
import com.up.constants.MT_APPS
import com.up.main.BaseActivity
import com.up.main.DashboardActivity
import com.up.main.Fragments.AppsFragment
import kotlinx.android.synthetic.main.item_header.view.*
import kotlinx.android.synthetic.main.row_apps_list.view.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

/**
 */

class StickyHeaderAdapterApps(var context: Context, var adapterItemSelectionListener: BaseActivity.AdapterItemSelectionListener,
                              var fragment: AppsFragment) : StickyHeaderGridAdapter() {
    var selectedFiles = ArrayList<String>()
    var checkedHeaders = HashSet<String>()
    var lastSelectedImageDrawable = ""
    var dataModel = ArrayList<DashboardActivity.MainDataList>()

    private val parentActivity = context as DashboardActivity
    var manager = parentActivity.packageManager

    override fun getSectionCount(): Int {
        return if (dataModel.size > 0) 1 else 0
    }

    override fun getSectionItemCount(section: Int): Int {
        return dataModel.size
    }

    override fun onCreateHeaderViewHolder(parent: ViewGroup, headerType: Int): StickyHeaderGridAdapter.HeaderViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_header, parent, false)
        return MyHeaderViewHolder(view)
    }

    override fun onCreateItemViewHolder(parent: ViewGroup, itemType: Int): StickyHeaderGridAdapter.ItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_apps_list, parent, false)
        return MyItemViewHolder(view)
    }

    override fun onBindHeaderViewHolder(viewHolder: StickyHeaderGridAdapter.HeaderViewHolder, section: Int) {
        val holder = viewHolder as MyHeaderViewHolder
        holder.itemView.txtHeader.text = dataModel[section].date
        holder.itemView.txtCount.text = "${dataModel.size} Apps Installed."

        holder.itemView.headerCheck.tag = section

        holder.itemView.headerCheck.isChecked = checkedHeaders.contains(holder.itemView.headerCheck.tag.toString())

        holder.itemView.headerCheck.setOnClickListener {
            val buttonView = it as CheckBox
            if (buttonView.isChecked) checkedHeaders.add(buttonView.tag.toString()) else checkedHeaders.remove(buttonView.tag.toString())
            toggleSectionSelection(buttonView.isChecked, buttonView.tag.toString().toInt())
        }
    }

    override fun onBindItemViewHolder(viewHolder: StickyHeaderGridAdapter.ItemViewHolder, section: Int, position: Int) {
        val holder = viewHolder as MyItemViewHolder
        val holderItem = dataModel[position]
        val item = holderItem.applicationInfo

        val requestOptions = RequestOptions()
                .override(150, 150)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .placeholder(ColorDrawable(parentActivity.getRandomColor()))

        Glide.with(context)
                .load(item?.icon)
                .apply(requestOptions)
                .into(holder.itemView.imgGrid)

        holder.itemView.selectFile.tag = "$section,$position"

        /*if (getLastElement() == holder.itemView.selectFile.tag && selectedFiles.size > 0) {
            lastSelectedImageDrawable = item.getString(item.getColumnIndex(MediaStore.Video.Media.DATA))
        }*/

        holder.itemView.selectFile.isChecked = selectedFiles.contains(holder.itemView.selectFile.tag.toString())
//        holder.itemView.layerSelected.visibility = if (selectedFiles.contains(holder.itemView.selectFile.tag.toString())) View.VISIBLE else View.GONE

        holder.itemView.txtName.text = item?.name
        holder.itemView.txtFileSize.text = "${parentActivity.getSizeInMB(item?.size!!)}"

        holder.itemView.selectFile.setOnClickListener {

            val toggle = holder.itemView.selectFile as CheckBox
//            toggle.isChecked = !toggle.isChecked

            if (toggle.isChecked) {
                selectedFiles.add(toggle.tag.toString())
            } else {
                selectedFiles.remove(toggle.tag.toString())
            }

            passDataToCounter(toggle.tag.toString(), toggle.isChecked)
        }

    }

    class MyHeaderViewHolder(itemView: View) : StickyHeaderGridAdapter.HeaderViewHolder(itemView)
    class MyItemViewHolder(itemView: View) : StickyHeaderGridAdapter.ItemViewHolder(itemView)


    fun refreshData(dataModel: ArrayList<DashboardActivity.MainDataList>) {
        this.dataModel.clear()
        this.dataModel.addAll(dataModel)
        notifyAllSectionsDataSetChanged()
    }

    fun toggleSectionSelection(isChecked: Boolean, section: Int) {

        selectedFiles = removeDuplicates(selectedFiles)

        for (i in 0 until dataModel.size) {
            if (isChecked)
                selectedFiles.add("$section,$i")
            else selectedFiles.remove("$section,$i")

            passDataToCounter("$section,$i", isChecked)
        }
    }

    fun removeDuplicates(data: ArrayList<String>): ArrayList<String> {

        val hashSet = TreeSet<String>()
        hashSet.addAll(data)
        data.clear()
        data.addAll(hashSet)

        return data
    }

    fun getLastElement(): String {
        return if (selectedFiles.size > 0) selectedFiles[selectedFiles.size - 1] else "0,0"
    }

    fun passDataToCounter(tagPosition: String, add: Boolean) {
        /** To get proper file from Cursor */
        async(UI) {
            val result = bg {
                val clickedPos = tagPosition.split(",")
                val clickedItem = dataModel[clickedPos[1].toInt()]

                val lastSelectedModel = DashboardActivity.LastSelectedModel(mimeType = MT_APPS, path = clickedItem.applicationInfo?.apkFile!!, icon = clickedItem.applicationInfo?.icon,
                        size = clickedItem.applicationInfo?.size!!,name = "${clickedItem.applicationInfo?.name!!}.apk")
                parentActivity.checkLastSelected(lastSelectedModel, add, tagPosition)
            }

            result.await()
            adapterItemSelectionListener.onItemSelected()
            notifyAllSectionsDataSetChanged()
        }
    }
}