package com.up.adapter

import android.content.Context
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.up.R
import com.up.data.Bean.FilesBean
import com.up.main.DashboardActivity
import kotlinx.android.synthetic.main.row_photos.view.*

class GridAdapter(val context: Context) : ListAdapter<FilesBean, RecyclerView.ViewHolder>(diffItems()) {

    private val parentActivity = context as DashboardActivity
    private val TYPE_HEADER = 0
    private val TYPE_CONTENT = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VH(LayoutInflater.from(context).inflate(R.layout.row_photos, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)

        /*if (holder is HeaderViewHolder) {
            holder.itemView.layHeader.visibility = View.VISIBLE
            holder.itemView.txtHeader.text = parentActivity.getDateFromTimeStamp(item.date_added.toString())
        } else {
            holder.itemView.layHeader.visibility = View.GONE
        }*/

        Glide.with(context)
                .load(item.path)
                .into(holder.itemView.imgGrid)

        holder.itemView.imgGrid.setOnClickListener {
            parentActivity.toast(item.name)
        }

    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0
                || (parentActivity.getDateFromTimeStamp(getItem(position).date_added.toString())
                        != parentActivity.getDateFromTimeStamp(getItem(position - 1).date_added.toString()))) TYPE_CONTENT else TYPE_HEADER
    }

    class diffItems() : DiffUtil.ItemCallback<FilesBean>() {
        override fun areItemsTheSame(oldItem: FilesBean?, newItem: FilesBean?): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: FilesBean?, newItem: FilesBean?): Boolean {
            return oldItem == newItem
        }

    }


    class HeaderViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview)
    class VH(itemview: View) : RecyclerView.ViewHolder(itemview)

}