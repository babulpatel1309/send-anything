package com.up.adapter

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.codewaves.stickyheadergrid.StickyHeaderGridAdapter
import com.up.R
import com.up.constants.MT_AUDIO
import com.up.main.BaseActivity
import com.up.main.DashboardActivity
import com.up.main.Fragments.AudioFragment
import kotlinx.android.synthetic.main.item_header.view.*
import kotlinx.android.synthetic.main.row_audio.view.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

/**
 */

class StickyHeaderAdapterAudio(var context: Context, var adapterItemSelectionListener: BaseActivity.AdapterItemSelectionListener, var fragment: AudioFragment) : StickyHeaderGridAdapter() {
    var selectedFiles = ArrayList<String>()
    var checkedHeaders = HashSet<String>()
    var lastSelectedImageDrawable = ""

    var dataModel = ArrayList<DashboardActivity.MainDataList>()

    private val parentActivity = context as DashboardActivity

    override fun getSectionCount(): Int {
        return dataModel.size
    }

    override fun getSectionItemCount(section: Int): Int {
        return if (section > -1) dataModel[section].data?.count!! else 0
    }

    override fun onCreateHeaderViewHolder(parent: ViewGroup, headerType: Int): StickyHeaderGridAdapter.HeaderViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_header, parent, false)
        return MyHeaderViewHolder(view)
    }

    override fun onCreateItemViewHolder(parent: ViewGroup, itemType: Int): StickyHeaderGridAdapter.ItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_audio, parent, false)
        return MyItemViewHolder(view)
    }

    override fun onBindHeaderViewHolder(viewHolder: StickyHeaderGridAdapter.HeaderViewHolder, section: Int) {
        val holder = viewHolder as MyHeaderViewHolder
        holder.itemView.txtHeader.text = dataModel[section].date
        holder.itemView.headerCheck.tag = section
        holder.itemView.txtCount.text = "${dataModel[section].data?.count} Audios"

        holder.itemView.headerCheck.isChecked = checkedHeaders.contains(holder.itemView.headerCheck.tag.toString())

        holder.itemView.headerCheck.setOnClickListener {
            val buttonView = it as CheckBox
            if (buttonView.isChecked) checkedHeaders.add(buttonView.tag.toString()) else checkedHeaders.remove(buttonView.tag.toString())
            toggleSectionSelection(buttonView.isChecked, buttonView.tag.toString().toInt())
        }
    }

    override fun onBindItemViewHolder(viewHolder: StickyHeaderGridAdapter.ItemViewHolder, section: Int, position: Int) {
        val holder = viewHolder as MyItemViewHolder
        val item = dataModel[section].data
        item?.moveToPosition(position)

        val albumArt = getAlbumArt(item?.getString(item.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID))!!)
        val requestOptions = RequestOptions().override(150, 150).diskCacheStrategy(DiskCacheStrategy.RESOURCE).placeholder(ColorDrawable(parentActivity.getRandomColor()))

        Glide.with(context)
                .load(if (albumArt.trim().isNotEmpty()) albumArt else R.drawable.ic_audio)
                .apply(requestOptions)
                .into(holder.itemView.imgGrid)


        holder.itemView.selectFile.tag = "$section,$position"

        if (getLastElement() == holder.itemView.selectFile.tag && selectedFiles.size > 0) {
            lastSelectedImageDrawable = item.getString(item.getColumnIndex(MediaStore.Video.Media.DATA))
        }

        holder.itemView.selectFile.isChecked = selectedFiles.contains(holder.itemView.selectFile.tag.toString())
        holder.itemView.txtName.text = item.getString(item.getColumnIndex(MediaStore.Video.Media.DISPLAY_NAME))
        holder.itemView.txtFileSize.text = parentActivity.getSizeInMB(item.getLong(item.getColumnIndex(MediaStore.Video.Media.SIZE)))
        holder.itemView.txtDuration.text = parentActivity.parseTime(item.getLong(item.getColumnIndex(MediaStore.Video.Media.DURATION)))

        holder.itemView.selectFile.setOnClickListener {
            val buttonView = it as CheckBox
            if (buttonView.isChecked) selectedFiles.add(buttonView.tag.toString()) else selectedFiles.remove(buttonView.tag.toString())
            passDataToCounter(buttonView.tag.toString(), buttonView.isChecked)

        }

    }

    class MyHeaderViewHolder(itemView: View) : StickyHeaderGridAdapter.HeaderViewHolder(itemView)
    class MyItemViewHolder(itemView: View) : StickyHeaderGridAdapter.ItemViewHolder(itemView)


    fun refreshData(dataModel: ArrayList<DashboardActivity.MainDataList>) {
        this.dataModel.clear()
        this.dataModel.addAll(dataModel)
        notifyAllSectionsDataSetChanged()
    }

    fun toggleSectionSelection(isChecked: Boolean, section: Int) {

        selectedFiles = removeDuplicates(selectedFiles)

        for (i in 0 until dataModel[section].data?.count!!) {
            if (isChecked)
                selectedFiles.add("$section,$i")
            else selectedFiles.remove("$section,$i")
        }

        adapterItemSelectionListener.onItemSelected()
        notifyAllSectionsDataSetChanged()
    }

    fun removeDuplicates(data: ArrayList<String>): ArrayList<String> {

        val hashSet = TreeSet<String>()
        hashSet.addAll(data)
        data.clear()
        data.addAll(hashSet)

        return data
    }

    fun getLastElement(): String {
        return if (selectedFiles.size > 0) selectedFiles[selectedFiles.size - 1] else "0,0"
    }

    fun getAlbumArt(albumID: String): String {

        var albumArtPath: String? = ""

        val scopeCursor = context.contentResolver.query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                arrayOf(MediaStore.Audio.Albums.ALBUM_ART), "${MediaStore.Audio.Albums._ID} =?",
                arrayOf(albumID), null)

        try {
            if (scopeCursor != null) {
                scopeCursor.moveToFirst()
                albumArtPath = scopeCursor.getString(scopeCursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (albumArtPath == null) albumArtPath = ""

        return albumArtPath

    }

    fun passDataToCounter(tagPosition: String, add: Boolean) {
        /** To get proper file from Cursor */
        val clickedPos = tagPosition.split(",")
        val clickedItem = dataModel[clickedPos[0].toInt()].data
        clickedItem?.moveToPosition(clickedPos[1].toInt())

        val lastSelectedModel = DashboardActivity.LastSelectedModel(MT_AUDIO, clickedItem?.getString(clickedItem.getColumnIndex(MediaStore.Audio.Media.DATA))!!,
                clickedItem.getLong(clickedItem.getColumnIndex(MediaStore.Audio.Media.SIZE)))
        parentActivity.checkLastSelected(lastSelectedModel, add, tagPosition)

        adapterItemSelectionListener.onItemSelected()
        notifyAllSectionsDataSetChanged()
    }

}