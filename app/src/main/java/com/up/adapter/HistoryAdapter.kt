package com.up.adapter

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Context.CLIPBOARD_SERVICE
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.support.design.widget.BottomSheetDialog
import android.support.v4.content.ContextCompat
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.up.R
import com.up.constants.*
import com.up.data.Bean.CreateFileTransferBean
import com.up.main.BaseActivity
import com.up.main.DashboardActivity
import com.up.main.Fragments.FragmentHistory
import com.up.utils.ConnectivityReceiver
import kotlinx.android.synthetic.main.bottom_sheet_share.view.*
import kotlinx.android.synthetic.main.row_history.view.*
import okhttp3.ResponseBody
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.jetbrains.anko.support.v4.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File


class HistoryAdapter(val context: Context, val fragmentHistory: FragmentHistory) : ListAdapter<CreateFileTransferBean, HistoryAdapter.VH>(diffItems()) {

    val baseActivity = context as BaseActivity
    private val parentActivity = context as DashboardActivity

    var downloadUpdate: DownloadUpdate? = null
    var uploadUpdate: DownloadUpdate? = null

    var downloadPosition = HashMap<String, Int>()

    init {
        EventBus.getDefault().register(this@HistoryAdapter)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(LayoutInflater.from(context).inflate(R.layout.row_history, parent, false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {

        val item = getItem(position)
        downloadPosition[item.id!!] = position

        validateHistoryItems(item, holder.itemView.txtStatus)

        holder.itemView.txtName.text = "up.labstack.com/${item.code}"

        val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.RESOURCE).placeholder(ColorDrawable(parentActivity.getRandomColor()))
        //Calculate file size
        var fileSize: Long = 0
        item.files?.forEach {
            fileSize += it?.size!!
        }

        holder.itemView.txtFileSize.text = "${baseActivity.getSizeInMB(fileSize)} (${item.files?.size} Files)"

        holder.itemView.imgGrid.scaleType = ImageView.ScaleType.FIT_XY
        holder.itemView.txtPercentage.visibility = View.GONE

        when (item.transferType) {

            TYPE_DOWNLOAD -> {
                holder.itemView.txtStatus.setTextColor(context.resources.getColor(R.color.black70))
                holder.itemView.imgUpload.setImageResource(R.drawable.ic_receive_normal)
                holder.itemView.imgUpload.setColorFilter(ContextCompat.getColor(context, R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN)

                holder.itemView.layProgress.visibility = View.GONE
                holder.itemView.layFinished.visibility = View.VISIBLE
                holder.itemView.btnUpload.visibility = View.VISIBLE

                when (item.downloaded) {

                    DOWNLOADED -> {

                        holder.itemView.imgGrid.scaleType = ImageView.ScaleType.CENTER_CROP
                        holder.itemView.txtStatus.visibility = View.VISIBLE
                        holder.itemView.txtStatus.setTextColor(context.resources.getColor(R.color.red_age_validation))

                        var thumbnailSet = false

                        item.files!!.forEach {
                            val file = File("$docPath${item.code}/${it!!.name}")
                            if (isImageOrVideo(it.name!!)
                                    && file.exists()) {

                                thumbnailSet = true

                                Glide.with(context)
                                        .load(file)
                                        .apply(requestOptions)
                                        .into(holder.itemView.imgGrid)
                            }
                        }

                        if (!thumbnailSet) {
                            Glide.with(context)
                                    .load(R.drawable.ic_file)
                                    .into(holder.itemView.imgGrid)
                        }

                    }

                    DOWNLOADING -> {

                        holder.itemView.layProgress.visibility = View.VISIBLE
                        holder.itemView.txtPercentage.visibility = View.VISIBLE

                        holder.itemView.btnUpload.visibility = View.GONE
                        holder.itemView.txtStatus.setTextColor(context.resources.getColor(R.color.red_age_validation))

                        downloadUpdate?.let {
                            if (it.id == item.id) {
                                holder.itemView.downloadProgress.isIndeterminate = false
                                holder.itemView.downloadProgress.progress = it.progress

                                holder.itemView.txtFileSize.text = "${progressCounter(it.progress, parentActivity.getSizeInMBOnly(item.size!!))} ${parentActivity.getSizeUnit(item.size!!)} of ${baseActivity.getSizeInMB(fileSize)} ${parentActivity.getSizeUnit(item.size!!)} (${item.files?.size} Files)"

                                if (it.progress <= 100)
                                    holder.itemView.txtPercentage.text = "${it.progress}%"
                                else holder.itemView.txtPercentage.text = "100%"
                            } else {
                                holder.itemView.downloadProgress.isIndeterminate = true
                            }
                        }

                        Glide.with(context)
                                .load(R.drawable.ic_file)
                                .into(holder.itemView.imgGrid)
                    }

                    FAILED -> {
                        holder.itemView.txtStatus.text = "Failed"
                        holder.itemView.txtStatus.visibility = View.VISIBLE

                        Glide.with(context)
                                .load(R.drawable.ic_file)
                                .into(holder.itemView.imgGrid)

                    }

                    REMAINING -> {
                        holder.itemView.txtStatus.text = "Queue"
                        holder.itemView.txtStatus.visibility = View.VISIBLE

                        holder.itemView.btnUpload.visibility = View.GONE
                        Glide.with(context)
                                .load(R.drawable.ic_file)
                                .into(holder.itemView.imgGrid)
                    }

                }

            }

            TYPE_UPLOAD -> {

                holder.itemView.layProgress.visibility = View.GONE
                holder.itemView.layFinished.visibility = View.VISIBLE
                holder.itemView.btnUpload.visibility = View.VISIBLE

                when (item.downloaded) {

                    UPLOADING -> {
                        if (!(context as DashboardActivity).isUploading) {
                            item.downloaded = DOWNLOADED
                            fragmentHistory.forceRefresh = true
                            fragmentHistory.historyViewModel.addItem(item)
                        }

                        holder.itemView.layProgress.visibility = View.VISIBLE
                        holder.itemView.btnUpload.visibility = View.GONE
                        holder.itemView.txtPercentage.visibility = View.VISIBLE

                        holder.itemView.imgUpload.setImageResource(R.drawable.ic_send_normal)
                        holder.itemView.imgUpload.setColorFilter(ContextCompat.getColor(context, R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN)

                        holder.itemView.imgGrid.scaleType = ImageView.ScaleType.CENTER_CROP
                        holder.itemView.txtStatus.setTextColor(context.resources.getColor(R.color.red_age_validation))
                        holder.itemView.txtStatus.visibility = View.VISIBLE

                        uploadUpdate?.let {
                            if (it.id == item.id) {
                                holder.itemView.downloadProgress.isIndeterminate = false
                                holder.itemView.downloadProgress.progress = it.progress
                                holder.itemView.txtFileSize.text = "${progressCounter(it.progress, parentActivity.getSizeInMBOnly(item.size!!))} ${parentActivity.getSizeUnit(item.size!!)} of ${baseActivity.getSizeInMB(fileSize)} ${parentActivity.getSizeUnit(item.size!!)} (${item.files?.size} Files)"

                                if (it.progress <= 100)
                                    holder.itemView.txtPercentage.text = "${it.progress}%"
                                else holder.itemView.txtPercentage.text = "100%"

                                progressCounter(it.progress, parentActivity.getSizeInMBOnly(item.size!!))
                            } else {
                                holder.itemView.downloadProgress.isIndeterminate = true
                            }
                        }

                        var thumbnailSet = false

                        item.files!!.forEach {
                            val file = File("${it!!.name}")
                            if (isImageOrVideo(it.name!!)
                                    && file.exists()) {

                                thumbnailSet = true

                                Log.e("LastSet", file.absolutePath)
                                Glide.with(context)
                                        .load(file)
                                        .apply(requestOptions)
                                        .into(holder.itemView.imgGrid)
                            }
                        }

                        if (!thumbnailSet) {
                            Glide.with(context)
                                    .load(R.drawable.ic_file)
                                    .into(holder.itemView.imgGrid)
                        }
                    }

                    FAILED -> {
                        holder.itemView.txtStatus.text = "Failed"
                        holder.itemView.imgUpload.setImageResource(R.drawable.ic_send_normal)
                        holder.itemView.imgUpload.setColorFilter(ContextCompat.getColor(context, R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN)

                        holder.itemView.imgGrid.scaleType = ImageView.ScaleType.CENTER_CROP

                        holder.itemView.txtStatus.visibility = View.VISIBLE

                        var thumbnailSet = false

                        item.files!!.forEach {
                            val file = File("${it!!.name}")
                            if (isImageOrVideo(it.name!!)
                                    && file.exists()) {

                                thumbnailSet = true

                                Log.e("LastSet", file.absolutePath)
                                Glide.with(context)
                                        .load(file)
                                        .apply(requestOptions)
                                        .into(holder.itemView.imgGrid)
                            }
                        }

                        if (!thumbnailSet) {
                            Glide.with(context)
                                    .load(R.drawable.ic_file)
                                    .into(holder.itemView.imgGrid)
                        }

                    }

                    else -> {
                        holder.itemView.imgUpload.setImageResource(R.drawable.ic_send_normal)
                        holder.itemView.imgUpload.setColorFilter(ContextCompat.getColor(context, R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN)

                        holder.itemView.imgGrid.scaleType = ImageView.ScaleType.CENTER_CROP

                        holder.itemView.txtStatus.setTextColor(context.resources.getColor(R.color.red_age_validation))
                        holder.itemView.txtStatus.visibility = View.VISIBLE

                        var thumbnailSet = false

                        item.files!!.forEach {
                            val file = File("${it!!.name}")
                            if (isImageOrVideo(it.name!!)
                                    && file.exists()) {

                                thumbnailSet = true

                                Log.e("LastSet", file.absolutePath)
                                Glide.with(context)
                                        .load(file)
                                        .apply(requestOptions)
                                        .into(holder.itemView.imgGrid)
                            }
                        }

                        if (!thumbnailSet) {
                            Glide.with(context)
                                    .load(R.drawable.ic_file)
                                    .into(holder.itemView.imgGrid)
                        }
                    }

                }

            }

        }

        holder.itemView.btnDelete.tag = position
        holder.itemView.btnDelete.setOnClickListener {
            if (item.downloaded == DOWNLOADING) {
                val downloadCancel = DownloadCancel(item.id, downloadUpdate?.downloadId)
                EventBus.getDefault().post(downloadCancel)
            }

            baseActivity.toast("Cancelled")

            val tag = it.tag as Int
            fragmentHistory.forceRefresh = true
            val canceledItem = getItem(tag)
            canceledItem.downloaded = FAILED
            baseActivity.historyViewModel.addItem(canceledItem)
        }

        holder.itemView.btnDelete.setOnClickListener {
            try{
                if (item.downloaded == DOWNLOADING) {
                    val downloadCancel = DownloadCancel(item.id, downloadUpdate?.downloadId)
                    EventBus.getDefault().post(downloadCancel)
                } else if (item.downloaded == UPLOADING) {
                    val downloadCancel = DownloadCancel(item.id, 0, CANCEL_UPLOAD)
                    EventBus.getDefault().post(downloadCancel)
                }

                baseActivity.toast("Cancelled")

                val tag = it.tag as Int
                fragmentHistory.forceRefresh = true
                val canceledItem = getItem(tag)
                canceledItem.downloaded = FAILED
                baseActivity.historyViewModel.addItem(canceledItem)
            }catch(e :Exception){
                e.printStackTrace()
            }

        }

        holder.itemView.cardHistory.setOnClickListener {
            copyToClipBoard("$APP_SHARE_BASE_URL${item.code}")
        }

        holder.itemView.btnMore.setOnClickListener {
            showBottomSheetDialog(item)
        }
    }


    class diffItems() : DiffUtil.ItemCallback<CreateFileTransferBean>() {
        override fun areItemsTheSame(oldItem: CreateFileTransferBean?, newItem: CreateFileTransferBean?): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: CreateFileTransferBean?, newItem: CreateFileTransferBean?): Boolean {
            return oldItem == newItem
        }

    }

    class VH(itemview: View) : RecyclerView.ViewHolder(itemview)

    fun isImageOrVideo(fileName: String): Boolean {
        var fileType = ""
        try {
            fileType = fileName.substring(fileName.lastIndexOf(".")).toLowerCase()
        } catch (e: Exception) {
            e.printStackTrace()
            fileType = ""
        }
        return (fileType == ".image" || fileType == ".png" || fileType == ".3gp"
                || fileType == ".jpg" || fileType == ".jpeg" || fileType == ".mp4"
                || fileType == ".mkv")
    }

    //TODO This is not unused method. Its bus event receiver. It will update the download progress.
    @Subscribe
    fun updateDownloadProgress(downloadUpdate: DownloadUpdate) {

        if (downloadUpdate.updateType == TYPE_DOWNLOAD) {
            this.downloadUpdate = downloadUpdate
            if (downloadPosition[downloadUpdate.id] != null)
                notifyItemChanged(downloadPosition[downloadUpdate.id]!!)
            else
                notifyDataSetChanged()
        } else if (downloadUpdate.updateType == TYPE_UPLOAD) {
            Log.e("Upload event", uploadUpdate?.progress.toString())
            this.uploadUpdate = downloadUpdate
            if (downloadPosition[downloadUpdate.id] != null)
                notifyItemChanged(downloadPosition[downloadUpdate.id]!!)
            else
                notifyDataSetChanged()
        }

    }

    fun validateHistoryItems(createFileTransferBean: CreateFileTransferBean?, txtExpiry: TextView) {

        try {
            if (createFileTransferBean != null) {
                val difference = fragmentHistory.getCurrentTimeStamp() - createFileTransferBean.timestamp
                val hoursToMills = fragmentHistory.getMillisecondsFromHours(createFileTransferBean.ttl?.toLong()!! + 1)

                Log.e("Time Validation", "Diff : $difference && HoursToMill : $hoursToMills")
                if ((hoursToMills - difference) <= 0 && createFileTransferBean.itemExpired < 1) {
                    //Delete it.
                    createFileTransferBean.itemExpired = 1
                    fragmentHistory.historyViewModel.addItem(createFileTransferBean)
                }

                txtExpiry.text = getExpiryTime((hoursToMills - difference))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun showBottomSheetDialog(shareLink: CreateFileTransferBean) {

        Log.e("Transfer Code", shareLink.code)

        val bottomSheetDialog = BottomSheetDialog(context)
        val bottomSheetView = LayoutInflater.from(context).inflate(R.layout.bottom_sheet_share, null)
        bottomSheetDialog.setContentView(bottomSheetView)

        if (shareLink.transferType == TYPE_DOWNLOAD) {
            bottomSheetView.btnDownloadDialog.visibility = View.VISIBLE
        }

        bottomSheetView.btnDownloadDialog.setOnClickListener {
            parentActivity.findTransfer(shareLink.code!!, TYPE_DOWNLOAD)
            bottomSheetDialog.dismiss()
        }

        bottomSheetView.btnShareDialog.setOnClickListener {
            shareLink("$APP_SHARE_BASE_URL${shareLink.code}")
            bottomSheetDialog.dismiss()
        }

        bottomSheetView.btnDeleteDialog.setOnClickListener {
            deleteLink(shareLink)
            bottomSheetDialog.dismiss()
        }

        bottomSheetDialog.show()
    }

    fun shareLink(shareLink: String) {
        try {
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(Intent.EXTRA_TEXT, shareLink)
            sendIntent.type = "text/plain"
            context.startActivity(Intent.createChooser(sendIntent, "Share Link"))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun copyToClipBoard(shareLink: String) {
        val clipboard = context.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = ClipData.newPlainText("", shareLink)
        clipboard.primaryClip = clipData
        fragmentHistory.toast("Link copied to clipboard.")
    }

    fun deleteLink(deleteCode: CreateFileTransferBean) {
        if (ConnectivityReceiver.isConnected()) {
            fragmentHistory.baseActivity.callWS()?.deleteLink("${ApiUrl.CREATE_TRANSFER}/${deleteCode.code}")?.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    fragmentHistory.toast("Error")
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    if (response.isSuccessful && fragmentHistory.isSuccess(response.code())) {
                        fragmentHistory.toast("Deleted")
                        deleteCode.itemExpired = 1
                        fragmentHistory.historyViewModel.addItem(deleteCode)
                    } else {
                        fragmentHistory.toast("Something went wrong. Please try again later.")
                        Log.e("Error", response.errorBody().toString())
                    }
                }

            })

        } else {
            fragmentHistory.toast(context.resources.getString(R.string.error_internet_down))
        }
    }

    fun getExpiryTime(ttl: Int): String {

        when (ttl + 1) {

            1 -> {
                return "Expires soon"
            }

            4 -> {
                return "${context.resources.getString(R.string.expires_in)} ${context.resources.getString(R.string.expire_4_hour)}"
            }

            12 -> {
                return "${context.resources.getString(R.string.expires_in)} ${context.resources.getString(R.string.expire_12_hour)}"
            }

            24 -> {
                return "${context.resources.getString(R.string.expires_in)} ${context.resources.getString(R.string.expire_1_day)}"
            }

            72 -> {
                return "${context.resources.getString(R.string.expires_in)} ${context.resources.getString(R.string.expire_3_day)}"
            }

            else -> {
                return "${context.resources.getString(R.string.expires_in)} ${context.resources.getString(R.string.expire_1_day)}"
            }
        }
    }

    fun getExpiryTime(ttl: Long): String {

        when {

            ttl in 0L..HOUR_1 -> {
                return "Expires soon"
            }

            ttl in HOUR_1..HOUR_4 -> {
                return "${context.resources.getString(R.string.expires_in)} ${context.resources.getString(R.string.expire_4_hour)}"
            }

            ttl in HOUR_4..HOUR_12 -> {
                return "${context.resources.getString(R.string.expires_in)} ${context.resources.getString(R.string.expire_12_hour)}"
            }

            ttl in HOUR_12..HOUR_24 -> {
                return "${context.resources.getString(R.string.expires_in)} ${context.resources.getString(R.string.expire_1_day)}"
            }

            ttl in HOUR_24..HOUR_72 -> {
                return "${context.resources.getString(R.string.expires_in)} ${context.resources.getString(R.string.expire_3_day)}"
            }

            else -> {
                return "${context.resources.getString(R.string.expires_in)} ${context.resources.getString(R.string.expire_1_day)}"
            }
        }
    }

    fun progressCounter(progress: Int, totalMb: Long): String {
        return try {
            val downloadedMb = (progress * totalMb) / 100.0f
            if (downloadedMb <= totalMb)
                downloadedMb.toString()
            else totalMb.toString()
        } catch (e: Exception) {
            ""
        }
    }
}