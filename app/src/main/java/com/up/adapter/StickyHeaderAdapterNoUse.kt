package com.up.adapter

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.codewaves.stickyheadergrid.StickyHeaderGridAdapter
import com.up.R
import com.up.data.Bean.FilesBean
import com.up.main.BaseActivity
import com.up.main.DashboardActivity
import com.up.utils.OperationResponder
import kotlinx.android.synthetic.main.item_header.view.*
import kotlinx.android.synthetic.main.row_photos.view.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

/**
 */

class StickyHeaderAdapterNoUse(var context: Context, var adapterItemSelectionListener: BaseActivity.AdapterItemSelectionListener) : StickyHeaderGridAdapter() {
    var sectionHeaders = ArrayList<String>()
    var finalDataList: MutableList<List<FilesBean>> = ArrayList()
    var selectedFiles = ArrayList<String>()
    var checkedHeaders = HashSet<String>()

    var lastSelectedImageDrawable = ""
    private val parentActivity = context as DashboardActivity

    override fun getSectionCount(): Int {
        return finalDataList.size
    }

    override fun getSectionItemCount(section: Int): Int {
        return if (section > -1) finalDataList[section].size else 0
    }

    override fun onCreateHeaderViewHolder(parent: ViewGroup, headerType: Int): StickyHeaderGridAdapter.HeaderViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_header, parent, false)
        return MyHeaderViewHolder(view)
    }

    override fun onCreateItemViewHolder(parent: ViewGroup, itemType: Int): StickyHeaderGridAdapter.ItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_photos, parent, false)
        return MyItemViewHolder(view)
    }

    override fun onBindHeaderViewHolder(viewHolder: StickyHeaderGridAdapter.HeaderViewHolder, section: Int) {
        val holder = viewHolder as MyHeaderViewHolder
        holder.itemView.txtHeader.text = sectionHeaders[section]
        holder.itemView.headerCheck.tag = section

        holder.itemView.headerCheck.isChecked = checkedHeaders.contains(holder.itemView.headerCheck.tag.toString())

        holder.itemView.headerCheck.setOnClickListener {
            val buttonView = it as CheckBox
            if (buttonView.isChecked) checkedHeaders.add(buttonView.tag.toString()) else checkedHeaders.remove(buttonView.tag.toString())
            toggleSectionSelection(buttonView.isChecked, buttonView.tag.toString().toInt())
        }
    }

    override fun onBindItemViewHolder(viewHolder: StickyHeaderGridAdapter.ItemViewHolder, section: Int, position: Int) {
        val holder = viewHolder as MyItemViewHolder
        val item = finalDataList[section][position]

        if (parentActivity.scrollIdle) {
            val requestOptions = RequestOptions().override(150, 150).diskCacheStrategy(DiskCacheStrategy.RESOURCE)
//            requestOptions.override(150, 150)
//            requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL)

            Glide.with(this@StickyHeaderAdapterNoUse.context)
                    .load(item.path)
                    .apply(requestOptions)
                    .into(holder.itemView.imgGrid)

            holder.itemView.selectFile.tag = "$section,$position"
            if (getLastElement() == holder.itemView.selectFile.tag && selectedFiles.size > 0) {
                lastSelectedImageDrawable = item.path
//                parentActivity.updateLastSelected(lastSelectedImageDrawable)
            }

            holder.itemView.selectFile.isChecked = selectedFiles.contains(holder.itemView.selectFile.tag.toString())
            holder.itemView.layerSelected.visibility = if (selectedFiles.contains(holder.itemView.selectFile.tag.toString())) View.VISIBLE else View.GONE


            holder.itemView.selectFile.setOnClickListener {
                val buttonView = it as CheckBox
                if (buttonView.isChecked) selectedFiles.add(buttonView.tag.toString()) else selectedFiles.remove(buttonView.tag.toString())
                adapterItemSelectionListener.onItemSelected()
                notifyAllSectionsDataSetChanged()
            }

        } else {
            Glide.with(this@StickyHeaderAdapterNoUse.context)
                    .load(ColorDrawable(parentActivity.getRandomColor()))
                    .into(holder.itemView.imgGrid)
        }

    }

    class MyHeaderViewHolder(itemView: View) : StickyHeaderGridAdapter.HeaderViewHolder(itemView)
    class MyItemViewHolder(itemView: View) : StickyHeaderGridAdapter.ItemViewHolder(itemView)

    fun refreshData(dataList: ArrayList<FilesBean>, operationResponder: OperationResponder? = null) {

        parentActivity.highLight("Total cells :${dataList.size}")

        async(UI) {
            val result = bg {
                finalDataList.clear()
                val selectedDataList = ArrayList<FilesBean>()
                val filterData = ArrayList<FilesBean>()
                filterData.addAll(dataList)

                getSectionsData(filterData)

                sectionHeaders.forEachIndexed { index, date ->

                    val content = ArrayList<FilesBean>()

                    filterData.forEachIndexed { index, filesBean ->


                        if (date == parentActivity.getDateFromTimeStamp(filesBean.date_added.toString())) {
                            if (!selectedDataList.contains(filesBean)) {
                                content.add(filesBean)
                                selectedDataList.add(filesBean)
                            }
                        }
                    }

                    filterData.removeAll(content)
                    if (content.isNotEmpty())
                        finalDataList.add(content)

                }
            }

            operationResponder?.OnComplete(result.await())
            notifyAllSectionsDataSetChanged()

        }

    }

    fun getSectionsData(filterData: ArrayList<FilesBean>): Collection<String> {
        val sortedArray = ArrayList<String>()
        filterData.forEach {
            if (!sortedArray.contains(parentActivity.getDateFromTimeStamp(it.date_added.toString())))
                sortedArray.add(parentActivity.getDateFromTimeStamp(it.date_added.toString()))
        }

        sectionHeaders.clear()
        sectionHeaders.addAll(sortedArray)

        return sectionHeaders
    }

    fun toggleSectionSelection(isChecked: Boolean, section: Int) {

        selectedFiles = removeDuplicates(selectedFiles)

        for (i in 0 until finalDataList[section].size) {
            if (isChecked)
                selectedFiles.add("$section,$i")
            else selectedFiles.remove("$section,$i")
        }

        adapterItemSelectionListener.onItemSelected()
        notifyAllSectionsDataSetChanged()
    }

    fun removeDuplicates(data: ArrayList<String>): ArrayList<String> {

        val hashSet = TreeSet<String>()
        hashSet.addAll(data)
        data.clear()
        data.addAll(hashSet)

        return data
    }

    fun getLastElement(): String {
        return if (selectedFiles.size > 0) selectedFiles[selectedFiles.size - 1] else "0,0"
    }

}