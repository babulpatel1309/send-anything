package com.up.adapter

import android.content.Context
import android.content.res.AssetFileDescriptor
import android.database.Cursor
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.provider.ContactsContract
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.codewaves.stickyheadergrid.StickyHeaderGridAdapter
import com.up.R
import com.up.constants.*
import com.up.main.BaseActivity
import com.up.main.DashboardActivity
import kotlinx.android.synthetic.main.item_header.view.*
import kotlinx.android.synthetic.main.row_contacts.view.*
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

/**
 */

class StickyHeaderAdapterContacts(var context: Context, var adapterItemSelectionListener: BaseActivity.AdapterItemSelectionListener) : StickyHeaderGridAdapter() {
    var checkedHeaders = HashSet<String>()
    var selectedFiles = ArrayList<String>()

    var lastSelectedImageDrawable = ""

    var dataModel = ArrayList<DashboardActivity.MainDataList>()

    private val parentActivity = context as DashboardActivity

    override fun getSectionCount(): Int {
        return dataModel.size
    }

    override fun getSectionItemCount(section: Int): Int {
        return if (section > -1) dataModel[section].data?.count!! else 0
    }

    override fun onCreateHeaderViewHolder(parent: ViewGroup, headerType: Int): StickyHeaderGridAdapter.HeaderViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_header, parent, false)
        return MyHeaderViewHolder(view)
    }

    override fun onCreateItemViewHolder(parent: ViewGroup, itemType: Int): StickyHeaderGridAdapter.ItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_contacts, parent, false)
        return MyItemViewHolder(view)
    }

    override fun onBindHeaderViewHolder(viewHolder: StickyHeaderGridAdapter.HeaderViewHolder, section: Int) {
        val holder = viewHolder as MyHeaderViewHolder
        holder.itemView.txtHeader.text = dataModel[section].date
        holder.itemView.txtCount.text = "${dataModel[section].data?.count} Contacts"
        holder.itemView.headerCheck.tag = section

        holder.itemView.headerCheck.isChecked = checkedHeaders.contains(holder.itemView.headerCheck.tag.toString())

        holder.itemView.headerCheck.setOnClickListener {
            val buttonView = it as CheckBox
            if (buttonView.isChecked) checkedHeaders.add(buttonView.tag.toString()) else checkedHeaders.remove(buttonView.tag.toString())
            toggleSectionSelection(buttonView.isChecked, buttonView.tag.toString().toInt())
        }
    }

    override fun onBindItemViewHolder(viewHolder: StickyHeaderGridAdapter.ItemViewHolder, section: Int, position: Int) {
        val holder = viewHolder as MyItemViewHolder
        val item = dataModel[section].data
        item?.moveToPosition(position)

        val requestOptions = RequestOptions().override(150, 150).diskCacheStrategy(DiskCacheStrategy.RESOURCE).placeholder(ColorDrawable(parentActivity.getRandomColor())).error(R.drawable.ic_user_contact)

        Glide.with(context)
                .load(item?.getString(item.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI)))
                .apply(requestOptions)
                .into(holder.itemView.imgGrid)

        holder.itemView.selectFile.tag = "$section,$position"
        holder.itemView.selectFile.isChecked = selectedFiles.contains(holder.itemView.selectFile.tag.toString())
        holder.itemView.txtName.text = item?.getString(item.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))

        holder.itemView.selectFile.setOnClickListener {
            val buttonView = it as CheckBox
            if (buttonView.isChecked) selectedFiles.add(buttonView.tag.toString()) else selectedFiles.remove(buttonView.tag.toString())

            passDataToCounter(buttonView.tag.toString(), buttonView.isChecked)

        }

    }

    class MyHeaderViewHolder(itemView: View) : StickyHeaderGridAdapter.HeaderViewHolder(itemView)
    class MyItemViewHolder(itemView: View) : StickyHeaderGridAdapter.ItemViewHolder(itemView)

    fun refreshData(dataModel: ArrayList<DashboardActivity.MainDataList>) {
        this.dataModel.clear()
        this.dataModel.addAll(dataModel)
        notifyAllSectionsDataSetChanged()
    }

    fun toggleSectionSelection(isChecked: Boolean, section: Int) {

        selectedFiles = removeDuplicates(selectedFiles)

        for (i in 0 until dataModel[section].data?.count!!) {
            if (isChecked)
                selectedFiles.add("$section,$i")
            else selectedFiles.remove("$section,$i")

            passDataToCounter("$section,$i", isChecked)
        }
    }

    fun removeDuplicates(data: ArrayList<String>): ArrayList<String> {

        val hashSet = TreeSet<String>()
        hashSet.addAll(data)
        data.clear()
        data.addAll(hashSet)

        return data
    }

    fun passDataToCounter(tagPosition: String, add: Boolean) {
        /** To get proper file from Cursor */
        val clickedPos = tagPosition.split(",")
        val clickedItem = dataModel[clickedPos[0].toInt()].data
        clickedItem?.moveToPosition(clickedPos[1].toInt())

        get(clickedItem, add)
        var contactsFileSize = 0L
        val file = File("$docPath$sharedDrive$CONTACT_VCF")
        if (file.exists()) contactsFileSize = file.length()

        val lastSelectedModel = DashboardActivity.LastSelectedModel(MT_CONTACT,
                clickedItem?.getString(clickedItem.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))!!,
                contactsFileSize, name = file.absolutePath)
        parentActivity.checkLastSelected(lastSelectedModel, add, tagPosition)

        adapterItemSelectionListener.onItemSelected()
        notifyAllSectionsDataSetChanged()
    }

    val vCard = ArrayList<String>()

    fun get(cursor: Cursor?, added: Boolean) {
        if (cursor?.count!! <= 0) return

        //cursor.moveToFirst();
        val lookupKey = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY))
        val uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_VCARD_URI, lookupKey)
        val fd: AssetFileDescriptor
        try {
            fd = context.contentResolver.openAssetFileDescriptor(uri, "r")

            val fileInputStream = FileInputStream(fd.fileDescriptor)
            val buf = ByteArray(fileInputStream.available())
            fileInputStream.read(buf)
            val vcardstring = String(buf)
            if (added)
                vCard.add(vcardstring)
            else vCard.remove(vcardstring)

            val mFileOutputStream = FileOutputStream("$docPath$sharedDrive$CONTACT_VCF", false)
            mFileOutputStream.write(getMergedContacts().toByteArray())

        } catch (e: Exception) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }
    }


    fun getMergedContacts(): String {
        return TextUtils.join("\n", vCard)
    }

}