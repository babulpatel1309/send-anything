package com.up.constants

import com.up.ApplicationClass

open class ApiUrl {

    /** API Listing*/

    companion object {

        var apiUrl: ApiUrl? = null
        const val CREATE_TRANSFER = "${APP_BASE_URL}transfers"

        init {
            if (apiUrl == null) apiUrl = ApiUrl()
        }

        fun getInstance(): ApiUrl {
            return apiUrl!!
        }

    }

    fun getAPI(type: String): String {

        return when (type) {
            MAKE_TRANSFER_READY -> {
                "${APP_BASE_URL}transfers/${ApplicationClass.app.getPrefs()?.getStringDetail(PARAM_CODE)}/ready"
            }
            SEND_FILES -> {
                "${APP_BASE_URL}transfers/${ApplicationClass.app.getPrefs()?.getStringDetail(PARAM_CODE)}/send"
            }
            RECEIVE_FILES -> {
                "${APP_BASE_URL}transfers/${ApplicationClass.app.getPrefs()?.getStringDetail(PARAM_CODE)}/receive"
            }
            FIND_TRANSFER -> {
                "${APP_BASE_URL}transfers/${ApplicationClass.app.getPrefs()?.getStringDetail(PARAM_CODE)}"
            }
            LOAD_QR_CODE -> {
                "${APP_BASE_URL}transfers/${ApplicationClass.app.getPrefs()?.getStringDetail(PARAM_CODE)}/code"
            }
            else -> {
                "${APP_BASE_URL}transfers"
            }
        }
    }

    fun getAPI(type: String, code: String): String {

        return when (type) {
            MAKE_TRANSFER_READY -> {
                "${APP_BASE_URL}transfers/$code/ready"
            }
            SEND_FILES -> {
                "${APP_BASE_URL}transfers/$code/send"
            }
            RECEIVE_FILES -> {
                "${APP_BASE_URL}transfers/$code/receive"
            }
            FIND_TRANSFER -> {
                "${APP_BASE_URL}transfers/$code"
            }
            LOAD_QR_CODE -> {
                "${APP_BASE_URL}transfers/$code/code"
            }
            else -> {
                "${APP_BASE_URL}transfers"
            }
        }
    }

}