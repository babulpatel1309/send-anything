package com.up.constants


const val ddMMyyyy = "dd-MM-yyyy"
const val ddMMMyyyy = "dd MMM yyyy"
const val MMMddyyyy = "MMM dd, yyyy"
const val ddMMyyyyHHmma = "dd-MM-yyyy hh:mm:ss a"
const val ddMMyyyyHHmm = "dd-MM-yyyy HH:mm:ss"
const val HHmma = "hh:mm a"
const val EEEE = "EEEE"
const val dd = "dd"

const val yyyyMMdd_HHmmssSSS = "yyyyMMdd_HHmmssSSS"


const val selection: String = ""

const val APP_BASE_URL = "https://api.labstack.com/up/"
const val APP_SHARE_BASE_URL = "https://up.labstack.com/"
const val PLAYSTORE_URL = "https://play.google.com/store/apps/details?id="

val HTTP_TIMEOUT: Long = 80
val success = 200

val SPLASH_TIMEOUT = 500L

const val BUNDLE_DATA = "DATA"
const val docPath = "/sdcard/Up/"
const val editImages = ".edit/"
const val sharedDrive = ".shared/"
const val compressedImages = ".compressed/"
const val CAMERA_FACING = "CAMERA_FACING"
const val stFlashMode = "FlashMode"
const val selectedImage = "selectedImage"
const val pickedImage = "pickedImage"
const val activityName = "activityName"
const val DATABASE_NAME = "headsupgamedb.db"
const val PKG_FB = "com.facebook.katana"
const val PKG_INSTAGRAM = "com.instagram.android"
const val PKG_GMAIL = "com.google.android.gm"

const val CONTACT_PICK = 10001
const val IMAGES_PICK = 10002
const val TAKE_PICTURE = 10003
const val DRIVE_PICK = 10004
const val EVENT_LISTENER = 10005
const val RC_REQUEST = 10006
const val EMAIL_SENT = 10007
const val CAMERA_PICK = 10008
const val RC_SIGN_IN = 10009

const val EVENT_CODE = 10011
const val EVENT_QR_CODE = 10012

const val CONTACTS = 101
const val STORAGE = 102
const val CAMERA = 103
const val AUDIO = 104

const val CONTACT_VCF = "Contacts.vcf"
const val WS_PARAM_CATID = "cat_id"

data class DownloadUpdate(
        var id: String,
        var progress: Int,
        var downloadId: Int,
        var updateType: Int
)

const val CANCEL_DOWNLOAD = 0
const val CANCEL_UPLOAD = 1

data class DownloadCancel(
        var id: String?,
        var downloadId: Int?,
        var cancelType: Int = CANCEL_DOWNLOAD
)

/** Download Status*/
const val REMAINING = -1
const val DOWNLOADED = 0
const val DOWNLOADING = 1
const val FAILED = 2
const val UPLOADING = 3

const val FROM_DEEP_LINKING = "deep_linking"

/** Download Status*/
const val TYPE_DOWNLOAD = 0
const val TYPE_UPLOAD = 1

/** Constants Theme*/
const val DATA_QR_RESULT = "qr_result"
const val PREF_FIRST_LAUNCH = "FIRST_LAUNCH"


/** Mime Types*/

const val MT_JPEG = "image"
const val MT_VIDEO = "video"
const val MT_AUDIO = "audio"
const val MT_APPS = "apps"
const val MT_CONTACT = "contact"

/** APIs*/

const val CREATE_TRANSFER = "CREATE_TRANSFER"
const val MAKE_TRANSFER_READY = "MAKE_TRANSFER_READY"
const val SEND_FILES = "SEND_FILESSEND_FILES"
const val RECEIVE_FILES = "RECEIVE_FILES"
const val FIND_TRANSFER = "FIND_TRANSFER"
const val LOAD_QR_CODE = "LOAD_QR_CODE"


/** API params*/

const val PARAM_NAME = "name"
const val PARAM_TYPE = "type"
const val PARAM_TTL = "ttl"
const val PARAM_SIZE = "size"
const val PARAM_FILES = "files"

const val TYPE_DIRECT = "direct"
const val TYPE_LINK = "link"
const val PARAM_CODE = "code"

/** Hours Limit*/

const val HOUR_1 = 3600000L
const val HOUR_4 = 14400000L
const val HOUR_12 = 43200000L
const val HOUR_24 = 86400000L
const val HOUR_72 = 259200000L
