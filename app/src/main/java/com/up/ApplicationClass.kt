package com.up

import android.app.Application
import android.content.ComponentCallbacks2
import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import com.crashlytics.android.Crashlytics
import com.facebook.FacebookSdk
import com.joanzapata.iconify.Iconify
import com.joanzapata.iconify.fonts.MaterialModule
import com.up.utils.Prefs
import io.fabric.sdk.android.Fabric
import java.util.*


/**
 * Created by sotsys-055 on 29/12/17.
 */
open class ApplicationClass : Application() {

    lateinit var appContext: Context

    var PREF_NAME = "applicationClass"
    private var prefs: Prefs? = null

    override fun onCreate() {
        super.onCreate()
        appContext = this
        mInstance = this
        app = this

        Iconify.with(MaterialModule())
        prefs = Prefs(getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE))
        Fabric.with(this, Crashlytics())
    }

    fun getPrefs(): Prefs? {
        return prefs
    }

    companion object {
        lateinit var mInstance: ApplicationClass
        lateinit var app: ApplicationClass

        @Synchronized
        fun getInstance(): ApplicationClass {
            return mInstance
        }

        fun getApps(): ApplicationClass {
            return app
        }
    }
}