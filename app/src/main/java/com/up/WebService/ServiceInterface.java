package com.up.WebService;

import com.up.constants.ApiUrl;
import com.up.data.Bean.CreateFileTransferBean;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Streaming;
import retrofit2.http.Url;


/**
 * Created by Harsh on 29/12/17.
 */
public interface ServiceInterface
{

    @POST(ApiUrl.CREATE_TRANSFER)
    Call<CreateFileTransferBean> createTransfer(@Body HashMap<String, Object> requestBody);

    @POST(ApiUrl.CREATE_TRANSFER)
    Call<ResponseBody> testPost(@Body HashMap<String, Object> requestBody);

    @GET
    Call<ResponseBody> makeTransfer(@Url String accessUrl);

    @Multipart
    @POST
    Call<ResponseBody> uploadAttachments(@Url String accessUrl, @Part MultipartBody.Part attachment);

    @Multipart
    @POST
    Call<ResponseBody> uploadAttachments(@Url String accessUrl, @Part List<MultipartBody.Part> attachment);

    @GET
    Call<CreateFileTransferBean> findTransfer(@Url String accessUrl);

    @Streaming
    @GET
    Call<ResponseBody> downloadFiles(@Url String accessUrl);

    @DELETE
    Call<ResponseBody> deleteLink(@Url String accessUrl);

   /*
    @GET("checkVersion.php")
    Call<VersionCheckBean> checkVersion();

    @Streaming
    @GET
    Call<ResponseBody> syncImages(@Url String fileUrl);

    @GET("getAdsSettings")
    Call<AdsSetting> getAdsSettingsNew(@QueryMap Map<String, String> requestBody);
*/
}